require('module-alias/register');
var assert = require('assert');
const {
    isTrue,
    isEmpty,
    inArray,
    isDefined,
    isInt,
    isFunction
} = require('@utils');

describe('Test util functions', function() {
    describe('isTrue', function () {
        it('should return true for true', function (done) {
            assert.equal(isTrue(true), true);
            done();
        });
        it('should return false for false', function (done) {
            assert.equal(isTrue(false), false);
            done();
        });
        it('should return true for "true"', function (done) {
            assert.equal(isTrue('true'), true);
            done();
        });
        it('should return false for "false"', function (done) {
            assert.equal(isTrue('false'), false);
            done();
        });
        it('should return true 1', function (done) {
            assert.equal(isTrue(1), true);
            done();
        });
        it('should return false for 0', function (done) {
            assert.equal(isTrue(true), true);
            done();
        });
    });

    describe('isEmpty', function () {
        it('should return true for undefined', function (done) {
            assert.equal(isEmpty(undefined), true);
            done();
        });
        it('should return true for null', function (done) {
            assert.equal(isEmpty(null), true);
            done();
        });
        it('should return true for []', function (done) {
            assert.equal(isEmpty([]), true);
            done();
        });
        it('should return true for ""', function (done) {
            assert.equal(isEmpty(''), true);
            done();
        });
        it('should return false for true', function (done) {
            assert.equal(isEmpty(true), false);
            done();
        });
        it('should return false for false', function (done) {
            assert.equal(isEmpty(false), false);
            done();
        });
        it('should return false for "string"', function (done) {
            assert.equal(isEmpty('string'), false);
            done();
        });
        it('should return false for [1, 2, 3]', function (done) {
            assert.equal(isEmpty([1, 2, 3]), false);
            done();
        });
    });

    describe('inArray', function () {
        it('should return true for 1 in [1, 2]', function (done) {
            assert.equal(inArray(1, [1, 2]), true);
            done();
        });
        it('should return false for "1" in [1, 2]', function (done) {
            assert.equal(inArray('1', [1, 2]), false);
            done();
        });
    });

    describe('isDefined', function () {
        it('should return false for undefined', function (done) {
            assert.equal(isDefined(undefined), false);
            done();
        });
        it('should return true for null', function (done) {
            assert.equal(isDefined(null), true);
            done();
        });
        it('should return true for ""', function (done) {
            assert.equal(isDefined(''), true);
            done();
        });
        it('should return true for 0', function (done) {
            assert.equal(isDefined(0), true);
            done();
        });
    });

    describe('isFunction', function () {
        it('should return false for undefined', function (done) {
            assert.equal(isFunction(undefined), false);
            done();
        });
        it('should return false for null', function (done) {
            assert.equal(isFunction(null), false);
            done();
        });
        it('should return false for ""', function (done) {
            assert.equal(isFunction(''), false);
            done();
        });
        it('should return false for 0', function (done) {
            assert.equal(isFunction(0), false);
            done();
        });
        it('should return true for ()=>({}))', function (done) {
            assert.equal(isFunction(()=>({})), true);
            done();
        });
    });

    describe('isInt', function () {
        it('should return false for undefined', function (done) {
            assert.equal(isInt(undefined), false);
            done();
        });
        it('should return false for null', function (done) {
            assert.equal(isInt(null), false);
            done();
        });
        it('should return false for ""', function (done) {
            assert.equal(isInt(''), false);
            done();
        });
        it('should return false for "0"', function (done) {
            assert.equal(isInt("0"), false);
            done();
        });
        it('should return true for 0', function (done) {
            assert.equal(isInt(0), true);
            done();
        });
    });
});
