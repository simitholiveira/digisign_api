require('module-alias/register');
var assert = require('assert');
const {
    isChildEvent,
    sanitizeInputParentId,
} = require('@utils/event');

describe('Test util event functions', function() {
    describe('isChildEvent', function () {
        it('should return false for 0', function (done) {
            assert.equal(isChildEvent(0), false);
            done();
        });
    });

    describe('sanitizeInputParentId', function () {
        it('should return null for ""', function (done) {
            assert.equal(sanitizeInputParentId(""), null);
            done();
        });
        it('should return 1 for 1', function (done) {
            assert.equal(sanitizeInputParentId(1), 1);
            done();
        });
        it('should return 1 for "1"', function (done) {
            assert.equal(sanitizeInputParentId("1"), 1);
            done();
        });
    });
});
