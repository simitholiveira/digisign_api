require('module-alias/register');
var assert = require('assert');
const {
    isHTMLByFileName,
    isImageByFileName,
    isVideoByFileName,
    isHTMLByMimeType,
    isImageByMimeType,
    isVideoByMimeType,
} = require('@utils/file');

describe('Test file util functions', function() {
    describe('isHTMLByFileName', function () {
        it('should return true for "file.html"', function (done) {
            assert.equal(isHTMLByFileName("file.html"), true);
            done();
        });
        it('should return false for "file.pdf"', function (done) {
            assert.equal(isHTMLByFileName("file.pdf"), false);
            done();
        });
    });

    describe('isImageByFileName', function () {
        it('should return true for "file.jpg"', function (done) {
            assert.equal(isImageByFileName("file.jpg"), true);
            done();
        });
        it('should return true for "file.png"', function (done) {
            assert.equal(isImageByFileName("file.png"), true);
            done();
        });
        it('should return false for "file.pdf"', function (done) {
            assert.equal(isImageByFileName("file.pdf"), false);
            done();
        });
    });

    describe('isVideoByFileName', function () {
        it('should return true for "file.mp4"', function (done) {
            assert.equal(isVideoByFileName("file.mp4"), true);
            done();
        });
        it('should return false for "file.pdf"', function (done) {
            assert.equal(isVideoByFileName("file.pdf"), false);
            done();
        });
    });

    describe('isHTMLByMimeType', function () {
        it('should return true for "text/html"', function (done) {
            assert.equal(isHTMLByMimeType("text/html"), true);
            done();
        });
        it('should return false for "text/xml"', function (done) {
            assert.equal(isHTMLByMimeType("text/xml"), false);
            done();
        });
    });

    describe('isImageByMimeType', function () {
        it('should return true for "image/jpg"', function (done) {
            assert.equal(isImageByMimeType("image/jpg"), true);
            done();
        });
        it('should return true for "image/png"', function (done) {
            assert.equal(isImageByMimeType("image/png"), true);
            done();
        });
        it('should return false for "application/pdf"', function (done) {
            assert.equal(isImageByMimeType("application/pdf"), false);
            done();
        });
    });

    describe('isVideoByMimeType', function () {
        it('should return true for "video/mp4"', function (done) {
            assert.equal(isVideoByMimeType("video/mp4"), true);
            done();
        });
        it('should return false for "audio/mp4"', function (done) {
            assert.equal(isVideoByMimeType("audio/mp4"), false);
            done();
        });
    });
});
