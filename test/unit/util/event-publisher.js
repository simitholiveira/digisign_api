require('module-alias/register');
var assert = require('assert');
const EventPublisher = require('@utils/event-publisher');

describe('Test EventPublisher', function() {
    it('throws TypeError if a listener is not a function', function () {

        const eventPublisher = new EventPublisher;

        assert.throws(() => {
            eventPublisher.on('somethingHappens', 'something')
        }, TypeError);

    });

    it('After event happened listener is informed', function () {

        const someData = 'someData';
        const eventPublisher = new EventPublisher;

        eventPublisher.on('somethingHappens', (props) => {
            assert.equal(props.someData, someData);
        });

        eventPublisher.notify('somethingHappens', {someData});
    });
});
