require('module-alias/register');
const assert = require('assert');
const expect = require('expect');
const Playlist = require('@models/playlist-model');
const Event = require('@models/event-model');
const { updateEvent } = require('@usecases/event/update-event');
const { NotFoundError } = require('@errors');
const EventRepository = require('@repos/event-repository');

const eventId = 1;

beforeEach(async function() {
    await Playlist.query().truncate();
    await Event.query().truncate();

    const fixtures = require("simple-knex-fixtures");

    const playlist = {
        table: 'playlist',
        data: [
            {
                external_id: 'playlist-1',
            },
        ],
    };
    await fixtures.loadFixture(playlist, Playlist.knex());

    const event = {
        table: 'event',
        data: [
            {
                id: eventId,
                start_date: "2020-03-12 18:03:55",
                end_date: "2020-03-13 18:03:55",
                text: "something",
                playlist_id: 1,
                event_length: 300,
                event_pid: 0,
                rec_type: "day_1____"
            },
        ],
    };
    await fixtures.loadFixture(event, Event.knex());
});

describe('Test Update Event Use Case', function() {

    it('throws NotFoundError trying to update a non-existent event.', async function () {

        const request = {
            params: {
                id: 2
            },
            body: {
                text: "some-text",
            }
        };

        await expect(updateEvent(request)).rejects.toThrow(NotFoundError);

    });

    it('throws NotFoundError trying to update an event with non-existent playlist.', async function () {

        const request = {
            params: {
                id: eventId
            },
            body: {
                content_type: {
                    type: 'playlist',
                    id: "non-existent-playlist"
                },
            }
        };

        await expect(updateEvent(request)).rejects.toThrow(NotFoundError);

    });

    it('updates existed event.', async function () {

        const text = "new-text";
        const request = {
            params: {
                id: eventId
            },
            body: {
                text,
            }
        };

        await updateEvent(request);

        const event = await EventRepository.findById(eventId);

        assert.equal(event.getText(), text);
    });

});
