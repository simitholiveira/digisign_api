require('module-alias/register');
var assert = require('assert');
var expect = require('expect');
const Playlist = require('@models/playlist-model');
const Event = require('@models/event-model');
const { createEvent } = require('@usecases/event/create-event');
const { NotFoundError } = require('@errors');
const EventRepository = require('@repos/event-repository');


describe('Test Create Event Use Case', function() {

    beforeEach(async function() {
        await Playlist.query().truncate();
        await Event.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const playlist = {
            table: 'playlist',
            data: [
                {
                    external_id: 'playlist-1',
                },
            ],
        };
        await fixtures.loadFixture(playlist, Playlist.knex());
    });

    it('throws NotFoundError trying to create an event with non-existent playlist.', async function () {

        const request = {
            body: {
                id: 1,
                start_date: "2020-03-12T18:03:55.241Z",
                end_date: "2020-03-13T18:03:55.241Z",
                text: "something",
                content_type: {
                    type: 'playlist',
                    id: "non-existent-playlist"
                },
                event_length: 300,
                event_pid: 0,
                rec_type: "day_1____"
            }
        };

        await expect(createEvent(request)).rejects.toThrow(NotFoundError);

    });

    it('creates new event.', async function () {

        const eventId = 1;
        const request = {
            body: {
                id: eventId,
                start_date: "2020-03-12T18:03:55.241Z",
                end_date: "2020-03-13T18:03:55.241Z",
                text: "something",
                content_type: {
                    type: 'playlist',
                    id: "playlist-1",
                },
                event_length: 300,
                event_pid: 0,
                rec_type: "day_1____"
            }
        };

        await createEvent(request);

        const event = await EventRepository.findById(eventId);

        assert(event);
    });

});
