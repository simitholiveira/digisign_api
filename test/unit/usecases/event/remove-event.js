require('module-alias/register');
var expect = require('expect');
const Playlist = require('@models/playlist-model');
const Event = require('@models/event-model');
const { removeEvent } = require('@usecases/event/remove-event');
const { NotFoundError } = require('@errors');
const EventRepository = require('@repos/event-repository');

const eventId = 1;

beforeEach(async function() {
    await Event.query().truncate();

    const fixtures = require("simple-knex-fixtures");

    const event = {
        table: 'event',
        data: [
            {
                id: eventId,
                start_date: "2020-03-12 18:03:55",
                end_date: "2020-03-13 18:03:55",
                text: "something",
                playlist_id: 1,
                event_length: 300,
                event_pid: 0,
                rec_type: "day_1____"
            },
        ],
    };
    await fixtures.loadFixture(event, Event.knex());
});

describe('Test Remove Event Use Case', function() {

    it('deletes existed event.', async function () {

        const request = {
            params: {
                id: eventId
            },
        };

        await removeEvent(request);

        await expect(EventRepository.findById(eventId)).rejects.toThrow(NotFoundError);
    });

});
