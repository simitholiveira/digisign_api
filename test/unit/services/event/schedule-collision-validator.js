require('module-alias/register');
var assert = require('assert');
const Event = require('@models/event-model');
const EventScheduleCollisionValidator = require('@api/validations/schedule-collision-validator');
const ScheduleCollisionChecker = require('@services/event/schedule-collision-checker');
const EventScheduleBuilderWrapper = require('@services/event/schedule-builders/event-schedule-builder-wrapper');
const ScheduleBuilderFactory = require('@services/event/schedule-builders/schedule-builder-factory');
const { ScheduleCollisionError } = require('@errors');


const scheduledEvent = new Event;
scheduledEvent.setId(1);
scheduledEvent.setStartAt("2020-10-01 00:00:00");
scheduledEvent.setEndAt("2020-10-20 01:00:00");
scheduledEvent.setRecurrenceType("day_2___");
scheduledEvent.setEventLength(90);

describe('Test EventScheduleCollisionValidator', function() {

    it('throws ScheduleCollisionError for day_1___.', function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-10-10 00:01:30");
        verifiableEvent.setEndAt("2020-10-12 00:05:00");
        verifiableEvent.setRecurrenceType("day_1___");
        verifiableEvent.setEventLength(120);

        const collisionValidator = new EventScheduleCollisionValidator(verifiableEvent);
        collisionValidator.setScheduleCollisionChecker(new ScheduleCollisionChecker);
        collisionValidator.setPotentialOverlappedEvents([scheduledEvent]);

        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        collisionValidator.setEventScheduleBuilder(eventScheduleBuilder);

        assert.throws(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

    it('throws ScheduleCollisionError for week_1___0.', function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-10-11 00:00:30");
        verifiableEvent.setEndAt("2020-12-12 00:05:00");
        verifiableEvent.setRecurrenceType("week_1___0");
        verifiableEvent.setEventLength(120);

        const collisionValidator = new EventScheduleCollisionValidator(verifiableEvent);
        collisionValidator.setScheduleCollisionChecker(new ScheduleCollisionChecker);
        collisionValidator.setPotentialOverlappedEvents([scheduledEvent]);

        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        collisionValidator.setEventScheduleBuilder(eventScheduleBuilder);

        assert.throws(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

    it('throws ScheduleCollisionError for month_1___.', function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-10-11 00:00:30");
        verifiableEvent.setEndAt("2020-12-12 00:05:00");
        verifiableEvent.setRecurrenceType("month_1___");
        verifiableEvent.setEventLength(120);

        const collisionValidator = new EventScheduleCollisionValidator(verifiableEvent);
        collisionValidator.setScheduleCollisionChecker(new ScheduleCollisionChecker);
        collisionValidator.setPotentialOverlappedEvents([scheduledEvent]);

        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        collisionValidator.setEventScheduleBuilder(eventScheduleBuilder);

        assert.throws(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

    it('does not throw ScheduleCollisionError for day_1___.', function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-10-10 00:01:31");
        verifiableEvent.setEndAt("2020-10-12 00:05:00");
        verifiableEvent.setRecurrenceType("day_1___");
        verifiableEvent.setEventLength(120);

        const collisionValidator = new EventScheduleCollisionValidator(verifiableEvent);
        collisionValidator.setScheduleCollisionChecker(new ScheduleCollisionChecker);
        collisionValidator.setPotentialOverlappedEvents([scheduledEvent]);

        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        collisionValidator.setEventScheduleBuilder(eventScheduleBuilder);

        assert.doesNotThrow(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

    it('does not throw ScheduleCollisionError for day_1___#2.', function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-09-29 00:00:30");
        verifiableEvent.setEndAt("2020-10-12 00:05:00");
        verifiableEvent.setRecurrenceType("day_1___#2");
        verifiableEvent.setEventLength(120);

        const collisionValidator = new EventScheduleCollisionValidator(verifiableEvent);
        collisionValidator.setScheduleCollisionChecker(new ScheduleCollisionChecker);
        collisionValidator.setPotentialOverlappedEvents([scheduledEvent]);

        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        collisionValidator.setEventScheduleBuilder(eventScheduleBuilder);

        assert.doesNotThrow(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

});
