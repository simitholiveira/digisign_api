require('module-alias/register');
var assert = require('assert');
const CurrentlyPlayableEvent = require('@services/event/currently-playable-event');
const Event = require('@models/event-model');

describe('Test CurrentlyPlayableEvent', function() {

    it('has correct workflow', function () {

        const currentlyPlayableEvent = new CurrentlyPlayableEvent;

        assert.equal(currentlyPlayableEvent.isFree(), true);

        const event1 = new Event;
        event1.setId(1);
        currentlyPlayableEvent.setEvent(event1);

        assert.equal(currentlyPlayableEvent.isCurrentEvent(event1), true);

        const event2 = new Event;
        event2.setId(2);

        assert.equal(currentlyPlayableEvent.isCurrentEvent(event2), false);
        assert.equal(currentlyPlayableEvent.isFree(), false);

        currentlyPlayableEvent.freeUp();

        assert.equal(currentlyPlayableEvent.isFree(), true);
    });

});
