require('module-alias/register');
var assert = require('assert');
const DailyScheduleBuilder = require('@services/event/schedule-builders/daily-schedule-builder');
const WeeklyScheduleBuilder = require('@services/event/schedule-builders/weekly-schedule-builder');
const MonthlyScheduleBuilder = require('@services/event/schedule-builders/monthly-schedule-builder');
const MockRecTypeParser = require('@services/event/rec-type-parsers/mock-rec-type-parser');
const moment = require('moment');


describe('Test ScheduleBuilders', function() {

    describe('Test WeeklyScheduleBuilder', function() {

        it('has correct workflow', function () {

            const recurrenceNWeek = 2;
            const mondayNumber = 1;
            const thursdayNumber = 4;

            const recTypeParser = new MockRecTypeParser('week', recurrenceNWeek);
            recTypeParser.setWeekDays([mondayNumber, thursdayNumber]);

            const unixStartAt = moment('2020-05-01 00:00:01').milliseconds(0).format('x') / 1000;
            const unixEndAt = moment('2020-05-10 23:59:59').milliseconds(0).format('x') / 1000;
            const eventLengthInSeconds = 60;

            const scheduleBuilder = new WeeklyScheduleBuilder(recTypeParser);
            scheduleBuilder.setStartEndUnixTimestamps(unixStartAt, unixEndAt);
            scheduleBuilder.setEventLength(eventLengthInSeconds);

            const schedule = scheduleBuilder.createSchedule();

            const expectedSchedule = [
                {
                    startAt: 1588539601,
                    endAt: 1588539661
                },
                {
                    startAt: 1588798801,
                    endAt: 1588798861
                }
            ];

            assert.deepEqual(schedule, expectedSchedule);
        });

    });

    describe('Test DailyScheduleBuilder', function() {

        it('has correct workflow', function () {

            const recurrenceNDay = 4;

            const recTypeParser = new MockRecTypeParser('day', recurrenceNDay);

            const unixStartAt = moment('2020-05-01 00:00:01').milliseconds(0).format('x') / 1000;
            const unixEndAt = moment('2020-05-07 23:59:59').milliseconds(0).format('x') / 1000;
            const eventLengthInSeconds = 60;

            const scheduleBuilder = new DailyScheduleBuilder(recTypeParser);
            scheduleBuilder.setStartEndUnixTimestamps(unixStartAt, unixEndAt);
            scheduleBuilder.setEventLength(eventLengthInSeconds);

            const schedule = scheduleBuilder.createSchedule();

            const expectedSchedule = [
                {
                    startAt: 1588280401,
                    endAt: 1588280461
                },
                {
                    startAt: 1588626001,
                    endAt: 1588626061
                }
            ];

            assert.deepEqual(schedule, expectedSchedule);
        });

    });

    describe('Test MonthlyScheduleBuilder', function() {

        it('has correct workflow', function () {

            const recurrenceNMonth = 2;

            const recTypeParser = new MockRecTypeParser('month', recurrenceNMonth);

            const unixStartAt = moment('2020-05-01 00:00:01').milliseconds(0).format('x') / 1000;
            const unixEndAt = moment('2020-07-10 23:59:59').milliseconds(0).format('x') / 1000;
            const eventLengthInSeconds = 60;

            const scheduleBuilder = new MonthlyScheduleBuilder(recTypeParser);
            scheduleBuilder.setStartEndUnixTimestamps(unixStartAt, unixEndAt);
            scheduleBuilder.setEventLength(eventLengthInSeconds);

            const schedule = scheduleBuilder.createSchedule();

            const expectedSchedule = [
                { startAt: 1588280401, endAt: 1588280461 },
                { startAt: 1593550801, endAt: 1593550861 }
            ];

            assert.deepEqual(schedule, expectedSchedule);
        });

    });

});
