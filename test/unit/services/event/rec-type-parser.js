require('module-alias/register');
var assert = require('assert');
const { InvalidDataError } = require('@errors');
const RecTypeParser = require('@services/event/rec-type-parsers/rec-type-parser');
const config = require('@config');

const availableRecTypes = config.event.availableRecurrenceTypes;
const minRecCount = config.event.minRecurrenceCount;
const maxRecCount = config.event.maxRecurrenceCount;

describe('Test RecTypeParser [type]_[count]_[day]_[count2]_[days]#[extra]', function() {

    describe('throws InvalidDataError if type', function() {

        it('is "something_1___"; whereas available types are: ' + availableRecTypes.join(', '), function () {

            assert.throws(() => {
                new RecTypeParser("type_1___")
            }, InvalidDataError);

        });

        it('are "day_0___", "day_____"; whereas count has to be in range between ' + minRecCount + ' and ' + maxRecCount, function () {

            assert.throws(() => {
                new RecTypeParser("day_0___")
            }, InvalidDataError);

            assert.throws(() => {
                new RecTypeParser("day_____")
            }, InvalidDataError);

        });

    });

    it('is correct if recurrence type is "day_1___"', function () {

        const recTypeParser = new RecTypeParser("day_1___");

        assert.equal(recTypeParser.getType(), 'day');
        assert.equal(recTypeParser.isDailyRecurrenceType(), true);
        assert.equal(recTypeParser.getCount(), 1);

    });

});
