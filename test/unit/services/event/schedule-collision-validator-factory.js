require('module-alias/register');
var assert = require('assert');
const Event = require('@models/event-model');
const { ScheduleCollisionError } = require('@errors');
const EventScheduleCollisionValidatorFactory = require('@api/validations/schedule-collision-validator-factory');
const EventRepository = require('@repos/event-repository');


describe('Test EventScheduleCollisionValidatorFactory', function() {

    beforeEach(async function() {
        await Event.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const event = {
            table: 'event',
            data: [
                {
                    id: 1,
                    playlist_id: 1,
                    start_date: '2020-10-01 00:00:00',
                    end_date: '2020-10-20 01:00:00',
                    rec_type: 'day_2___',
                    event_length: 90,
                    text: 'something',
                    event_pid: 0
                },
            ],
        };
        await fixtures.loadFixture(event, Event.knex());
    });

    it('throws ScheduleCollisionError if there is at least one event with overlapped schedule.', async function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-10-10 00:01:30");
        verifiableEvent.setEndAt("2020-10-12 00:05:00");
        verifiableEvent.setRecurrenceType("day_1___");
        verifiableEvent.setEventLength(120);

        const collisionValidatorFactory = new EventScheduleCollisionValidatorFactory(verifiableEvent);
        collisionValidatorFactory.setPotentialOverlappedEventsProvider(async event => {
            return await EventRepository.findAllOverlappedByEvent(event);
        });
        const collisionValidator = await collisionValidatorFactory.createValidator();

        assert.throws(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

    it('does not throw ScheduleCollisionError if there is none event with overlapped schedule.', async function () {

        const verifiableEvent = new Event;
        verifiableEvent.setId(2);
        verifiableEvent.setStartAt("2020-10-10 00:01:31");
        verifiableEvent.setEndAt("2020-10-12 00:05:00");
        verifiableEvent.setRecurrenceType("day_1___");
        verifiableEvent.setEventLength(120);

        const collisionValidatorFactory = new EventScheduleCollisionValidatorFactory(verifiableEvent);
        collisionValidatorFactory.setPotentialOverlappedEventsProvider(async event => {
            return await EventRepository.findAllOverlappedByEvent(event);
        });
        const collisionValidator = await collisionValidatorFactory.createValidator();

        assert.doesNotThrow(() => {
            collisionValidator.assertCollisionIsAbsent()
        }, ScheduleCollisionError);

    });

});
