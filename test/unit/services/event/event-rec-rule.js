require('module-alias/register');
var assert = require('assert');
const { createEventRecurrenceRules } = require('@services/event/rec-rule-builders/event-rec-rule-builder');
const Event = require('@models/event-model');


describe('Test createEventRecurrenceRule', function() {

    it('returns true matching "32 15 08 */2 * ?" and rule for rec_type "day_2___" and time "2020-07-15 08:15:32"', function () {

        const event = new Event;
        event.setStartAt("2020-07-15 08:15:32");
        event.setRecurrenceType("day_2___");

        const [rule] = createEventRecurrenceRules(event);

        assert.equal(rule, "32 15 08 */2 * ?");

    });

    it('has correct workflow for rec_type "week_2___1,4"', function () {

        const event = new Event;
        event.setStartAt("2020-05-01 00:00:01");
        event.setEndAt("2020-05-10 23:59:59");
        event.setRecurrenceType("week_2___1,4");
        event.setEventLength(60);

        const rules = createEventRecurrenceRules(event);
        const expectedRules = ['2020-05-04 00:00:01', '2020-05-07 00:00:01'];

        assert.deepEqual(rules, expectedRules);

    });

});
