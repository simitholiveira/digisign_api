var assert = require('assert');

const Slide = require('@models/slide-model');
const Playlist = require('@models/playlist-model');
const PlaylistSlide = require('@models/playlist-slide-model');

const PlaylistRepository = require('@repos/playlist-repository');
const PlaylistService = require('@services/playlist-service');

const playlistService = new PlaylistService(new PlaylistRepository);

beforeEach(async function() {
    await Slide.query().truncate();
    await Playlist.query().truncate();
    await PlaylistSlide.query().truncate();

    const fixtures = require("simple-knex-fixtures");
    const slides = {
        table: 'slide',
        data: [
            {
                external_id: 'slide_1',
                duration: 10,
            },
        ],
    };
    await fixtures.loadFixture(slides, Slide.knex());
});

describe('Playlist service', function () {
    describe('Create playlist', function () {
        it('Playlist is created', async function() {

            const playlistAttributes = {
                id: 'pl_1'
            };
            const playlistSlide = PlaylistSlide.create(1, 10);

            await playlistService.createPlaylist(playlistAttributes, [playlistSlide]);

            const playlist = await PlaylistRepository.findByExternalId(playlistAttributes.id);

            assert(playlist);
            assert.equal(playlist.playlistSlides.length, 1);
            assert(playlist.playlistSlides[0].id);
        })
    })
});
