const assert = require('assert');
const fs = require('fs-extra');
const SlideRepository = require('@repos/slide-repository');
const {SlideService} = require('@services/slide-service');
const Slide = require('@models/slide-model');
const File = require('@models/file-model');

const slideService = new SlideService(new SlideRepository);

const basePath = __dirname + '/../../fixtures';

beforeEach(async function() {
    await Slide.query().truncate();
    await File.query().truncate();
});

describe('Slide Service', function() {
    describe('Create Slide', function() {
        it('Slide is created with html only', async function() {

            const slideAttributes = {
                id: 'test1',
                durationInSeconds: 10
            };
            const htmlFile = getHtmlFile();

            await slideService.createSlide(slideAttributes, htmlFile);

            const slide = await SlideRepository.findByExternalId(slideAttributes.id);

            assert(slide);
            assert(slide.id);
            assert.equal(slide.files.length, 1);
            assert(slide.newestHtml);
            assert(slide.newestHtml.id);
        });

        it('Slide is created with images', async function() {

            const slideAttributes = {
                id: 'test2',
                durationInSeconds: 10
            };
            const htmlFile = getHtmlFile();
            const mediaFiles = getMediaFiles();

            await slideService.createSlide(slideAttributes, htmlFile, mediaFiles);

            const slide = await SlideRepository.findByExternalId(slideAttributes.id);

            assert(slide);
            assert.equal(slide.files.length, 3);
            assert(slide.newestHtml);
            assert.equal(slide.media.length, 2);
        });
    });

    describe('Update slide', function() {
        it('Images are updated', async function() {

            const slideAttributes = {
                id: 'test3',
                durationInSeconds: 10
            };
            const mediaFiles = getMediaFiles();
            const htmlFile = getHtmlFile();
            const sizeBefore = fs.statSync(mediaFiles[0].path);

            await slideService.createSlide(slideAttributes, htmlFile, mediaFiles);
            const slide = await SlideRepository.findByExternalId(slideAttributes.id);

            const real = getRealImage();
            const sizeAfter = fs.statSync(real.path);
            await slideService.updateFiles(slide, [real]);

            assert.equal(slide.media.length, 2);

            const image = slide.getFileByName(real.name);

            assert(image);
            assert.equal(image.size, real.size);
            assert.notEqual(sizeBefore['size'], sizeAfter['size']);
        });
    });
});

function getHtmlFile() {
    const html = File.create('default-page.html', 100, 'text/html');
    html.path = basePath + '/default-page.html';
    return html;
}

function getMediaFiles() {
    const image1 = File.create('blank1.jpg', 0, 'image/jpeg');
    image1.path = basePath + '/blank.jpg';
    const image2 = File.create('blank2.jpg', 0, 'image/jpeg');
    image2.path = basePath + '/blank.jpg';

    return [image1, image2];
}

function getRealImage() {
    const image = File.create('blank1.jpg', 10, 'image/jpeg');
    image.path = basePath + '/real.jpg';
    return image;
}
