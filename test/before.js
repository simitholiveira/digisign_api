require('module-alias/register');
const fs = require('fs-extra');
const config = require('@config');

let knex = null;

before(async function() {
    fs.emptyDir(__dirname + '/storage/upload');

    knex = require('@src/bootstrap').db(config.db);
    await knex.migrate.latest();
});
