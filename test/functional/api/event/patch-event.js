require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Playlist = require('@models/playlist-model');
const Event = require('@models/event-model');
const EventRepository = require('@repos/event-repository');
const assert = require('assert');

const should = chai.should();

chai.use(chaiHttp);

describe('Test PATCH Event', function() {

    const eventId = 1;
    const playlistExternalId = 'playlist-1';

    beforeEach(async function() {
        await Playlist.query().truncate();
        await Event.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const playlist = {
            table: 'playlist',
            data: [
                {
                    external_id: playlistExternalId,
                },
            ],
        };
        await fixtures.loadFixture(playlist, Playlist.knex());
        const event = {
            table: 'event',
            data: [
                {
                    id: eventId,
                    start_date: "2020-03-10 19:00:00",
                    end_date: "2020-03-23 20:00:00",
                    text: "event 1",
                    playlist_id: 1,
                    content_type: '{"type": "playlist", "id": 1}',
                    event_length: 60,
                    event_pid: 0,
                    rec_type: "day_1____",
                    rec_pattern: "day_1____",
                    time: "time",
                    play_content: "content",
                }
            ],
        };
        await fixtures.loadFixture(event, Playlist.knex());
    });

    describe('/PATCH event', function() {

        describe('Incorrect upcoming request', function() {

            describe('Incorrect "id" property', function() {
                it('should return 400 Bad Request because "eventId" has to be an integer.', function (done) {

                    const event = {
                        start_date: "2020-03-10 19:05:55",
                    };

                    chai.request(server)
                        .patch('/api/event/eventId')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "id" is non-existent.', function (done) {

                    const event = {
                        start_date: "2020-03-10 19:05:55",
                    };

                    chai.request(server)
                        .patch('/api/event/0')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "content_type" property', function() {
                it('should return 400 Bad Request for non-existent playlist.', function (done) {
                    const event = {
                        content_type: {
                            type: 'playlist',
                            id: 'non-existent-playlist'
                        },
                    };

                    chai.request(server)
                        .patch('/api/event/' + eventId)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "event_length" property', function() {
                it('should return 400 Bad Request because "event_length" has to be an integer.', function (done) {
                    const event = {
                        event_length: 'event_length',
                    };

                    chai.request(server)
                        .patch('/api/event/' + eventId)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "event_pid" property', function() {
                it('should return 400 Bad Request because "event_pid" has to be an integer.', function (done) {
                    const event = {
                        event_pid: 'event_pid',
                    };

                    chai.request(server)
                        .patch('/api/event/' + eventId)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                /**
                 * TODO it will be changed when child event is done.
                 */
                it('should return 400 Bad Request because "event_pid" has to be 0.', function (done) {
                    const event = {
                        event_pid: 1,
                    };

                    chai.request(server)
                        .patch('/api/event/' + eventId)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "rec_type" property', function() {
                it('should return 400 Bad Request because "rec_type" is incorrect.', function (done) {
                    const event = {
                        rec_type: "some_rec_type"
                    };

                    chai.request(server)
                        .patch('/api/event/' + eventId)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "rec_pattern" property', function() {
                it('should return 400 Bad Request because "rec_pattern" is incorrect.', function (done) {
                    const event = {
                        rec_pattern: "some_rec_pattern"
                    };

                    chai.request(server)
                        .patch('/api/event/' + eventId)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Schedule collision', function() {

                beforeEach(async function() {

                    const fixtures = require("simple-knex-fixtures");

                    const event = {
                        table: 'event',
                        data: [
                            {
                                id: 2,
                                start_date: "2020-03-10 18:00:00",
                                end_date: "2020-03-23 20:00:00",
                                text: "event 2",
                                playlist_id: 1,
                                content_type: '{"type": "playlist", "id": 1}',
                                event_length: 60,
                                event_pid: 0,
                                rec_type: "day_1____",
                                rec_pattern: "day_1____",
                                time: "time",
                                play_content: "content",
                            }
                        ],
                    };
                    await fixtures.loadFixture(event, Playlist.knex());
                });

                it('should return 400 Bad Request because of schedule collision.', function (done) {
                    const event = {
                        start_date: "2020-03-23 18:01:00"
                    };

                    chai.request(server)
                        .patch('/api/event/' + 1)
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            function badResponse(res) {
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.be.an('object');
                res.body.should.have.property('error');
            }
        });

        describe('Correct request', function() {

            it('should return success after modifying event.', function (done) {

                const text = "new-text";
                const event = {
                    text,
                };

                chai.request(server)
                    .patch('/api/event/' + eventId)
                    .set(config.api.accessKeyName, config.api.accessKeyValue)
                    .send(event)
                    .end(async (err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        res.body.should.have.property('success');

                        const event = await EventRepository.findById(eventId);

                        assert.equal(event.getText(), text);

                        done();
                    });

            });
        });
    });

});
