require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Playlist = require('@models/playlist-model');
const Event = require('@models/event-model');

const should = chai.should();

chai.use(chaiHttp);

function getEventTemplate() {
    return {
        id: 1,
        start_date: "2020-03-10 19:05:55",
        end_date: "2020-03-23 19:05:55",
        text: "event 1",
        content_type: {
            type: 'playlist',
            id: 1
        },
        event_length: 16,
        event_pid: 0,
        rec_type: "day_1____",
        rec_pattern: "day_1____",
        time: "time",
        play_content: "content",
    };
}

function postRequest(url, data) {
    return chai.request(server)
        .post(url)
        .set(config.api.accessKeyName, config.api.accessKeyValue)
        .send(data);
}

function badResponse(res) {
    res.should.have.status(400);
    res.should.be.json;
    res.body.should.be.an('object');
    res.body.should.have.property('error');
}

function successResponse(res) {
    res.should.have.status(200);
    res.should.be.json;
    res.body.should.be.an('object');
    res.body.should.have.property('success');
}

describe('Test POST Event', function() {

    const playlistExternalId = 'playlist-1';

    beforeEach(async function() {
        await Playlist.query().truncate();
        await Event.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const playlist = {
            table: 'playlist',
            data: [
                {
                    external_id: playlistExternalId,
                },
            ],
        };
        await fixtures.loadFixture(playlist, Playlist.knex());
    });

    describe('/POST event', function() {

        describe('Incorrect upcoming request', function() {

            describe('Incorrect "id" property', function() {

                beforeEach(async function() {

                    const fixtures = require("simple-knex-fixtures");

                    const event = {
                        table: 'event',
                        data: [
                            {
                                id: 1,
                                start_date: "2020-03-10 19:05:55",
                                end_date: "2020-03-23 19:05:55",
                                text: "event 1",
                                playlist_id: 1,
                                content_type: '{"type": "playlist", "id": 1}',
                                event_length: 16,
                                event_pid: 0,
                                rec_type: "day_1____"
                            }
                        ],
                    };
                    await fixtures.loadFixture(event, Event.knex());
                });

                it('should return 400 Bad Request because "id" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.id;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "id" has to be an integer.', function (done) {

                    const event = getEventTemplate();
                    event.id = 'id';

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "id" is existent.', function (done) {

                    const event = getEventTemplate();

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "start_date" property', function() {
                it('should return 400 Bad Request because "start_date" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.start_date;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "end_date" property', function() {
                it('should return 400 Bad Request because "end_date" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.end_date;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "text" property', function() {
                it('should return 400 Bad Request because "text" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.text;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "content_type" property', function() {
                it('should return 400 Bad Request because "content_type" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.content_type;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request for non-existent playlist.', function (done) {

                    const event = getEventTemplate();
                    event.content_type.id = 'non-existent-playlist';

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "event_length" property', function() {
                it('should return 400 Bad Request because "event_length" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.event_length;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "event_length" has to be an integer.', function (done) {

                    const event = getEventTemplate();
                    event.event_length = 'event_length';

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "event_pid" property', function() {
                it('should return 400 Bad Request because "event_pid" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.event_pid;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "event_pid" has to be an integer.', function (done) {

                    const event = getEventTemplate();
                    event.event_pid = 'event_pid';

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                /**
                 * TODO it will be changed when child event is done.
                 */
                it('should return 400 Bad Request because "event_pid" has to be 0.', function (done) {

                    const event = getEventTemplate();
                    event.event_pid = 1;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "rec_type" property', function() {
                it('should return 400 Bad Request because "rec_type" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.rec_type;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "rec_type" is incorrect.', function (done) {

                    const event = getEventTemplate();
                    event.rec_type = 'some_rec_type';

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "rec_pattern" property', function() {
                it('should return 400 Bad Request because "rec_pattern" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.rec_pattern;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "rec_pattern" is incorrect.', function (done) {

                    const event = getEventTemplate();
                    event.rec_pattern = 'some_rec_pattern';

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "time" property', function() {
                it('should return 400 Bad Request because "time" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.time;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Incorrect "play_content" property', function() {
                it('should return 400 Bad Request because "play_content" is required.', function (done) {

                    const event = getEventTemplate();
                    delete event.play_content;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });

            describe('Schedule collision', function() {

                beforeEach(async function() {

                    const fixtures = require("simple-knex-fixtures");

                    const event = {
                        table: 'event',
                        data: [
                            {
                                id: 1,
                                start_date: "2020-03-10 19:00:00",
                                end_date: "2020-03-23 20:00:00",
                                text: "event 1",
                                playlist_id: 1,
                                content_type: '{"type": "playlist", "id": 1}',
                                event_length: 60,
                                event_pid: 0,
                                rec_type: "day_1____",
                                rec_pattern: "day_1____",
                                time: "time",
                                play_content: "content",
                            }
                        ],
                    };
                    await fixtures.loadFixture(event, Event.knex());
                });

                it('should return 400 Bad Request because of schedule collision.', function (done) {

                    const event = getEventTemplate();
                    event.id = 2;
                    event.start_date = "2020-03-23 19:01:00";
                    event.end_date = "2020-03-28 20:00:00";
                    event.event_length = 60;

                    postRequest('/api/event', event)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });
            });
        });

        describe('Correct request', function() {

            it('should return success after creating event.', function (done) {

                const event = getEventTemplate();
                event.content_type.id = playlistExternalId;

                postRequest('/api/event', event)
                    .end(async (err, res) => {
                        successResponse(res);
                        done();
                    });
            });

            it('should return success after creating event.', function (done) {

                const event = {
                    id: 1,
                    start_date: "2020-04-10 19:05:55",
                    end_date: "2020-04-23 19:05:55",
                    text: "event 1",
                    content_type: {
                        type: 'playlist',
                        id: playlistExternalId
                    },
                    event_length: "",
                    event_pid: "",
                    rec_type: "",
                    rec_pattern: "",
                    time: "time",
                    play_content: "content",
                };

                postRequest('/api/event', event)
                    .end(async (err, res) => {
                        successResponse(res);
                        done();
                    });
            });
        });
    });

});
