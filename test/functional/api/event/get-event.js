require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Event = require('@models/event-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test GET Event', function() {

    beforeEach(async function() {
        await Event.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const event = {
            table: 'event',
            data: [
                {
                    id: 1,
                    start_date: "2020-03-10 19:05:55",
                    end_date: "2020-03-23 19:05:55",
                    text: "event 1",
                    playlist_id: 1,
                    content_type: '{"type": "playlist", "id": 1}',
                    event_length: 16,
                    event_pid: 0,
                    rec_type: "day_1____"
                }
            ],
        };
        await fixtures.loadFixture(event, Event.knex());
    });

    describe('/GET event', function() {

        it('should return event by its id.', function (done) {

            chai.request(server)
                .get('/api/event/1')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.an('object').and.not.empty;
                    done();
                });

        });

        it('should return 404 Not Found for non-existent event.', function (done) {

            chai.request(server)
                .get('/api/event/0')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.be.an('object');
                    res.body.should.have.property('error');
                    done();
                });

        });

    });

});
