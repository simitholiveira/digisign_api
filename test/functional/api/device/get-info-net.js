require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const should = chai.should();

chai.use(chaiHttp);

describe('Test Device', function() {

    describe('/GET device', function() {

        this.timeout(5000);

        it('should return device info.', function (done) {

            chai.request(server)
                .get('/api/device/info/net')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.an('object').and.not.empty;
                    done();
                });

        });

    });

});
