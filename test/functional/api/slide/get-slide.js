require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Slide = require('@models/slide-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test GET Slide', function() {

    beforeEach(async function() {
        await Slide.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const slide = {
            table: 'slide',
            data: [
                {
                    external_id: 'slide-1',
                    duration: 10,
                },
            ],
        };
        await fixtures.loadFixture(slide, Slide.knex());
    });

    describe('/GET slides', function() {

        it('should return all the slides.', function (done) {

            chai.request(server)
                .get('/api/slide')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('array');
                    done();
                });

        });

    });

});
