require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Slide = require('@models/slide-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test GET Slides', function() {

    beforeEach(async function() {
        await Slide.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const slide = {
            table: 'slide',
            data: [
                {
                    external_id: 'slide-1',
                    duration: 10,
                },
            ],
        };
        await fixtures.loadFixture(slide, Slide.knex());
    });

    describe('/GET slide', function() {

        it('should return slide by its external ID.', function (done) {

            chai.request(server)
                .get('/api/slide/slide-1')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.an('object').and.not.empty;
                    done();
                });

        });

        it('should return 404 Not Found for non-existent slide.', function (done) {

            chai.request(server)
                .get('/api/slide/non-existent-slide')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.be.an('object');
                    res.body.should.have.property('error');
                    done();
                });

        });

    });

});
