require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Slide = require('@models/slide-model');
const fs = require('fs-extra');

const should = chai.should();
const pathToFixtures = __dirname + '/../../../fixtures';

chai.use(chaiHttp);

describe('Test POST Slide', function() {

    beforeEach(async function() {
        await Slide.query().truncate();
    });

    describe('/POST slide', function() {

        describe('Incorrect upcoming request', function() {

            describe('Incorrect "id" property', function() {

                beforeEach(async function() {

                    const fixtures = require("simple-knex-fixtures");

                    const slide = {
                        table: 'slide',
                        data: [
                            {
                                external_id: 'slide-1',
                                duration: 10,
                            },
                        ],
                    };
                    await fixtures.loadFixture(slide, Slide.knex());
                });

                it('should return 400 Bad Request because "id" is required.', function (done) {

                    const slide = {
                        duration: 40
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "id" cannot be blank.', function (done) {

                    const slide = {
                        id: '',
                        durationInSeconds: 40
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request for existent slide.', function (done) {

                    const slide = {
                        id: 'slide-1',
                        durationInSeconds: 40
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });
            });

            describe('Incorrect "durationInSeconds" property', function() {

                it('should return 400 Bad Request because "durationInSeconds" is required.', function (done) {

                    const slide = {
                        id: 'slide-1'
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "durationInSeconds" has to be integer.', function (done) {

                    const slide = {
                        id: 'slide-1',
                        durationInSeconds: 'duration'
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request because "durationInSeconds" has to be greater than 0.', function (done) {

                    const slide = {
                        id: 'slide-1',
                        durationInSeconds: 0
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

            });

            describe('Incorrect "html" property', function() {

                it('should return 400 Bad Request because "html" is required.', function (done) {

                    const slide = {
                        id: 'slide-1',
                        durationInSeconds: 10,
                    };

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(slide)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "html" has to have html extension.', function (done) {

                    chai.request(server)
                        .post('/api/slide')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .field('name', 'slide-1')
                        .field('duration', 10)
                        .attach('html', fs.readFileSync(pathToFixtures + '/blank.jpg'), 'blank.jpg')
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

            });

            function badResponse(res) {
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.be.an('object');
                res.body.should.have.property('error');
            }
        });

    });

});
