require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Playlist = require('@models/playlist-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test GET Playlists', function() {

    beforeEach(async function() {
        await Playlist.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const playlist = {
            table: 'playlist',
            data: [
                {
                    external_id: 'playlist-1',
                },
            ],
        };
        await fixtures.loadFixture(playlist, Playlist.knex());
    });

    describe('/GET playlists', function() {

        it('should return all the playlists.', function (done) {

            chai.request(server)
                .get('/api/playlist')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('array').and.not.empty;
                    done();
                });

        });

    });

});
