require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Playlist = require('@models/playlist-model');
const PlaylistSlide = require('@models/playlist-slide-model');
const Slide = require('@models/slide-model');

const should = chai.should();

chai.use(chaiHttp);

function putRequest(url, data) {
    return chai.request(server)
        .put('/api/playlist')
        .set(config.api.accessKeyName, config.api.accessKeyValue)
        .send(data);
}

function badResponse(res) {
    res.should.have.status(400);
    res.should.be.json;
    res.body.should.be.an('object');
    res.body.should.have.property('error');
}

function successResponse(res, details = {}) {
    const {code = 200} = details;
    res.should.have.status(code);
    res.should.be.json;
    res.body.should.be.an('object');
    res.body.should.have.property('success');
}

describe('Test PUT Playlist', function() {

    beforeEach(async function() {
        await PlaylistSlide.query().truncate();
        await Playlist.query().truncate();
    });

    describe('/PUT playlist', function() {

        describe('Incorrect upcoming request', function() {

            describe('Incorrect "id" property', function() {

                beforeEach(async function() {

                    const fixtures = require("simple-knex-fixtures");

                    const playlist = {
                        table: 'playlist',
                        data: [
                            {
                                external_id: 'playlist-1',
                            },
                        ],
                    };
                    await fixtures.loadFixture(playlist, Playlist.knex());
                });

                it('should return 400 Bad Request because "id" is required.', function (done) {

                    const playlist = {
                        slides: [
                            {
                                id: "some-slide",
                                duration: 4
                            },
                        ]
                    };

                    putRequest('/api/playlist', playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "id" cannot be empty.', function (done) {

                    const playlist = {
                        id: "",
                        slides: [
                            {
                                id: "some-slide",
                                duration: 4
                            },
                        ]
                    };

                    putRequest('/api/playlist', playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });
            });

            describe('Incorrect "slides" property', function() {

                it('should return 400 Bad Request because "slides" is required.', function (done) {

                    const playlist = {
                        id: 'playlist'
                    };

                    putRequest('/api/playlist', playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "slides" can not be blank.', function (done) {

                    const playlist = {
                        id: 'playlist',
                        slides: []
                    };

                    putRequest('/api/playlist', playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "id" of slide is required.', function (done) {

                    const playlist = {
                        id: "playlist-2",
                        slides: [{}]
                    };

                    putRequest('/api/playlist', playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request for non-existent slide.', function (done) {

                    const playlist = {
                        id: "playlist-2",
                        slides: [
                            {
                                id: "non-existent-slide",
                            },
                        ]
                    };

                    putRequest('/api/playlist', playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

            });


        });

        describe('Correct creating request', function() {

            const slideExternalId = 'slide-1';

            beforeEach(async function() {
                await Slide.query().truncate();

                const fixtures = require("simple-knex-fixtures");

                const slides = [
                    {
                        table: 'slide',
                        data: [
                            {
                                external_id: slideExternalId,
                                duration: 10,
                            },
                        ],
                    },
                ];
                slides.map(async slide => await fixtures.loadFixture(slide, Slide.knex()));
            });

            it('should return success after creating playlist.', function (done) {

                const playlist = {
                    id: "playlist-name",
                    slides: [
                        {
                            id: slideExternalId,
                        },
                    ]
                };

                putRequest('/api/playlist', playlist)
                    .end(async (err, res) => {
                        successResponse(res, {code: 201});
                        done();
                    });

            });

        });

        describe('Correct updating request', function() {

            const slideExternalId1 = 'slide-1';
            const slideExternalId2 = 'slide-2';

            beforeEach(async function() {
                await Slide.query().truncate();

                const fixtures = require("simple-knex-fixtures");

                const slides = [
                    {
                        table: 'slide',
                        data: [
                            {
                                external_id: slideExternalId1,
                                duration: 10,
                            },
                        ],
                    },
                    {
                        table: 'slide',
                        data: [
                            {
                                external_id: slideExternalId2,
                                duration: 10,
                            },
                        ],
                    },
                ];
                slides.map(async slide => await fixtures.loadFixture(slide, Slide.knex()));

                const playlist = {
                    table: 'playlist',
                    data: [
                        {
                            external_id: 'playlist-name',
                        },
                    ],
                };
                await fixtures.loadFixture(playlist, Playlist.knex());
            });

            it('should return success after updating playlist.', function (done) {

                const playlist = {
                    id: "playlist-name",
                    slides: [
                        {
                            id: slideExternalId2,
                        },
                    ]
                };

                putRequest('/api/playlist', playlist)
                    .end(async (err, res) => {
                        successResponse(res);
                        done();
                    });

            });
        });

    });

});
