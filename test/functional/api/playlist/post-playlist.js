require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Playlist = require('@models/playlist-model');
const Slide = require('@models/slide-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test POST Playlist', function() {

    beforeEach(async function() {
        await Playlist.query().truncate();
    });

    describe('/POST playlist', function() {

        describe('Incorrect upcoming request', function() {

            describe('Incorrect "id" property', function() {

                beforeEach(async function() {

                    const fixtures = require("simple-knex-fixtures");

                    const playlist = {
                        table: 'playlist',
                        data: [
                            {
                                external_id: 'playlist-1',
                            },
                        ],
                    };
                    await fixtures.loadFixture(playlist, Playlist.knex());
                });

                it('should return 400 Bad Request because "id" is required.', function (done) {

                    const playlist = {
                        slides: [
                            {
                                id: "some-slide",
                                duration: 4
                            },
                        ]
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "id" cannot be empty.', function (done) {

                    const playlist = {
                        id: "",
                        slides: [
                            {
                                id: "some-slide",
                                duration: 4
                            },
                        ]
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request for existent playlist.', function (done) {

                    const playlist = {
                        id: "playlist-1",
                        slides: [
                            {
                                id: "some-slide",
                                duration: 4
                            },
                        ]
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });
            });

            describe('Incorrect "slides" property', function() {

                it('should return 400 Bad Request because "slides" is required.', function (done) {

                    const playlist = {
                        id: 'playlist'
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "slides" can not be blank.', function (done) {

                    const playlist = {
                        id: 'playlist',
                        slides: []
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

                it('should return 400 Bad Request because "id" of slide is required.', function (done) {

                    const playlist = {
                        id: "playlist-2",
                        slides: [{}]
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });
                });

                it('should return 400 Bad Request for non-existent slide.', function (done) {

                    const playlist = {
                        id: "playlist-2",
                        slides: [
                            {
                                id: "non-existent-slide",
                            },
                        ]
                    };

                    chai.request(server)
                        .post('/api/playlist')
                        .set(config.api.accessKeyName, config.api.accessKeyValue)
                        .send(playlist)
                        .end(async (err, res) => {
                            badResponse(res);
                            done();
                        });

                });

            });

            function badResponse(res) {
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.be.an('object');
                res.body.should.have.property('error');
            }
        });

        describe('Correct request', function() {

            const slideExternalId = 'slide-1';

            beforeEach(async function() {
                await Slide.query().truncate();

                const fixtures = require("simple-knex-fixtures");

                const slide = {
                    table: 'slide',
                    data: [
                        {
                            external_id: slideExternalId,
                            duration: 10,
                        },
                    ],
                };
                await fixtures.loadFixture(slide, Slide.knex());
            });

            it('should return success after creating playlist.', function (done) {

                const playlist = {
                    id: "playlist-name",
                    slides: [
                        {
                            id: slideExternalId,
                        },
                    ]
                };

                chai.request(server)
                    .post('/api/playlist')
                    .set(config.api.accessKeyName, config.api.accessKeyValue)
                    .send(playlist)
                    .end(async (err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        res.body.should.have.property('success');
                        done();
                    });

            });
        });

    });

});
