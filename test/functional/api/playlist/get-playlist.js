require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Playlist = require('@models/playlist-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test GET Playlist', function() {

    beforeEach(async function() {
        await Playlist.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const playlist = {
            table: 'playlist',
            data: [
                {
                    external_id: 'playlist-1',
                },
            ],
        };
        await fixtures.loadFixture(playlist, Playlist.knex());
    });

    describe('/GET playlist', function() {

        it('should return playlist by its external ID.', function (done) {

            chai.request(server)
                .get('/api/playlist/playlist-1')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.an('object').and.not.empty;
                    done();
                });

        });

        it('should return 404 Not Found for non-existent playlist.', function (done) {

            chai.request(server)
                .get('/api/playlist/non-existent-playlist')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.be.an('object');
                    res.body.should.have.property('error');
                    done();
                });

        });

    });

});
