require('module-alias/register');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('@root/bin/app');
const config = require('@config');
const Event = require('@models/event-model');

const should = chai.should();

chai.use(chaiHttp);

describe('Test GET Events List', function() {

    beforeEach(async function() {
        await Event.query().truncate();

        const fixtures = require("simple-knex-fixtures");

        const event = {
            table: 'event',
            data: [
                {
                    id: 1,
                    start_date: "2020-03-10 19:05:55",
                    end_date: "2020-03-23 19:05:55",
                    text: "event 1",
                    playlist_id: 1,
                    content_type: '{"type": "playlist", "id": 1}',
                    event_length: 16,
                    event_pid: 0,
                    rec_type: "day_1____"
                }
            ],
        };
        await fixtures.loadFixture(event, Event.knex());
    });

    describe('/GET events list', function() {

        it('should return all the events.', function (done) {

            chai.request(server)
                .get('/api/events-list')
                .set(config.api.accessKeyName, config.api.accessKeyValue)
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.an('array').and.not.empty;
                    done();
                });

        });

    });

});
