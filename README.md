#### Requirements
- node js
- npm
- mysql server
- chrome advacned (https://browser.taokaizen.com)

Chrome advacned is used instead of chromium because chromium does not play mp4 files.

#### Instructions
- Copy `config.js.dist` and name it `config.js`. Configure application. Create database.
- Run `npm install`
- Start dev server `npm run dev`
- Start events scheduling `node src/scheduler/event` (server has to work in order for browser to be able to display slides)
- If your server address is localhost and port is 3000, then api is accessible
via http://localhost:3000/api, swagger is accessible via http://localhost:3000/api-docs

#### Commands
- `npm run migrate` run migrations
- `npm run dev` run dev server
- `npx knex migrate:make <name>` create migration
- `npx knex -h` to see available knex commands

#### Debuging
0. Run application with --inspect flag (example: node --inspect ./bin/app.js)
0. In Chrome go to chrome://inspect
0. Click `Inspect` link
0. Set breakpoints and enjoy
