require('module-alias/register');
const app = require('@root/src/api');
const debug = require('debug')('api');
const http = require('http');
const config = require('@root/config');
const dnssd = require('dnssd2');
const Browser = require('@src/services/chrome-service');
const {getUniqueDeviceId, getDeviceIp4Address} = require('@src/services/device-service');

const port = normalizePort(config.api.port);
app.set('port', port);

const server = http.createServer(app);
server.listen(port);
server.timeout = config.server.timeoutInMilliseconds;
server.on('error', onError);
server.on('listening', onListening);

module.exports = server; // for testing

if (config.chrome.launchOnStartup) {
    Browser.showDefaultScreen();
    // const window = new Chrome();
    // window.launch(config.chrome.defaultUrl)
    //     .then(chrome => {
    //         app.locals.window = chrome;
    //     });
}

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
async function onListening() {

    const unitId = await getUniqueDeviceId();
    const broadcastString = config.zeroConf.name.replace('{unitId}', unitId);
    const deviceIp4 = await getDeviceIp4Address();
    const dnssdAdvertisement = new dnssd.Advertisement(dnssd.tcp('http'), config.api.port, {
        name: broadcastString,
        interface: deviceIp4
    });
    dnssdAdvertisement.start();

    // console.log('unitId: ', unitId);
    // console.log('Device ID: ', broadcastString);
    // console.log('Device ip4: ', deviceIp4);

    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}
