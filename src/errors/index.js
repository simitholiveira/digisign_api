class NotFoundError extends Error {
    constructor(message) {
        message = message || 'Not Found';
        super(message);
    }
}

class DuplicateEntityError extends Error {
    constructor(message) {
        message = message || 'Duplicate Entity';
        super(message);
    }
}

class UnprocessableEntityError extends Error {
    constructor(message) {
        message = message || 'Duplicate Entity';
        super(message);
    }
}

class InvalidDataError extends Error {
    constructor(message, details = {}) {
        message = message || 'Invalid Data';
        super(message);
        this.details = details;
    }
    getDetails() {
        return this.details;
    }
}

class InvalidConfigurationError extends Error {
    constructor(message) {
        message = message || 'Invalid Configuration';
        super(message);
    }
}

class ScheduleCollisionError extends Error {
    constructor(message) {
        message = message || 'Schedule Collision';
        super(message);
    }
}

module.exports = {
    NotFoundError,
    DuplicateEntityError,
    UnprocessableEntityError,
    InvalidDataError,
    InvalidConfigurationError,
    ScheduleCollisionError,
};
