
exports.up = function(knex) {
    return knex.schema.alterTable('slide', function(table) {
        table.integer('duration').unsigned().notNullable().after('name');
    });
};

exports.down = function(knex) {
    return knex.schema.alterTable('slide', function(table) {
        table.dropColumn('duration');
    });
};
