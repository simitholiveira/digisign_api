
exports.up = function(knex) {
    return knex.schema.alterTable('slide', function(table) {
        table.string('name', 100).notNullable().defaultTo('').after('external_id');
        table.string('thumbnail', 100).notNullable().defaultTo('').after('name');
        table.integer('highest_z_index').unsigned().notNullable().defaultTo('0').after('duration');
        table.text('assets').nullable().after('highest_z_index');
        table.text('styles').nullable().after('assets');
        table.text('components').nullable().after('styles');
        table.text('css').nullable().after('components');
    });
};

exports.down = function(knex) {
    return knex.schema.alterTable('slide', function(table) {
        table.dropColumn('name');
        table.dropColumn('thumbnail');
        table.dropColumn('highest_z_index');
        table.dropColumn('assets');
        table.dropColumn('styles');
        table.dropColumn('components');
        table.dropColumn('css');
    });
};
