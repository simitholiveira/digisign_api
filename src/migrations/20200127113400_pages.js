// const knex = require('knex');
/**
 */
exports.up = function (knex) {
    return knex.schema.createTable('slide', table => {
        table.increments('id').unsigned().notNullable();
        table.string('name', 50).notNullable().unique('name');
        table.timestamps(true, true);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('slide');
};
