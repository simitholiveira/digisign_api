
exports.up = function(knex) {
    return knex.schema.alterTable('slide', function(table) {
        table.renameColumn('name', 'external_id');
    });
};

exports.down = function(knex) {
    return knex.schema.alterTable('slide', function(table) {
        table.renameColumn('external_id', 'name');
    });
};
