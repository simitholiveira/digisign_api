
exports.up = function(knex) {
    return knex.schema.createTable('user', table => {
        table.increments('id').unsigned().notNullable();
        table.string('username', 50).notNullable();
        table.string('password_hash', 2048).notNullable();
        table.string('access_token', 2048).nullable().defaultTo(null);
        table.timestamps(true, true);

        table.unique(['username'], 'username');
        // table.index(['access_token'], 'access_token');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('user');
};
