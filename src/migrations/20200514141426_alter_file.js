
exports.up = function(knex) {
    return knex.schema
        .alterTable('file', function(table) {
            table.dropUnique(['slide_id', 'name'], 'slide_file');
            table.string('system_name', 1000).notNullable().after('name');
        })
        .raw('update `file` set `system_name`=`name`');
};

exports.down = function(knex) {
    return knex.schema.alterTable('file', function(table) {
        table.dropColumn('system_name');
    });
};
