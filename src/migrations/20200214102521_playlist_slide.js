
exports.up = function(knex) {
    return knex.schema.createTable('playlist_slide', function(table) {
        table.increments('id').unsigned().notNullable();
        table.integer('playlist_id').unsigned().notNullable();
        table.integer('slide_id').unsigned().notNullable();
        table.integer('duration').unsigned().notNullable();
        table.timestamps(true, true);

        table.index(['slide_id'], 'slide_id');
        table.index(['playlist_id', 'slide_id'], 'p_slide');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('playlist_slide');
};
