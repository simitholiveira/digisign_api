
exports.up = function(knex) {
    return knex.schema.raw('alter table `event` modify column `time` varchar(255) NOT NULL default "" after `rec_type`');
};

exports.down = function(knex) {
    return knex.schema.alterTable('event', function(table) {
        knex.schema.raw('alter table `event` modify column `time` varchar(25) NOT NULL default "" after `rec_type`');
    });
};
