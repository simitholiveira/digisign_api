
exports.up = function(knex) {
    return knex.schema.alterTable('event', function(table) {
        table.string('content_type', 50).notNullable().defaultTo('').after('playlist_id');
    });
};

exports.down = function(knex) {
    return knex.schema.alterTable('event', function(table) {
        table.dropColumn('content_type');
    });
};
