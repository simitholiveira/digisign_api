exports.up = function (knex) {
    return knex.schema.createTable('schedule', table => {
        table.increments('id').unsigned().notNullable();
        table.integer('slide_id').unsigned().notNullable();
        table.timestamp('start_at').nullable().defaultTo(null);
        table.timestamp('stop_at').nullable().defaultTo(null);
        table.timestamps(true, true);

        table.index('slide_id', 'slide_id');
        table.index('start_at', 'start_at');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('schedule');
};
