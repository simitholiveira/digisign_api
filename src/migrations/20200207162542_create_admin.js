const { hashPassword } = require('../util/crypto');

exports.up = function(knex) {
    return knex('user').insert({
        username: 'admin',
        password_hash: hashPassword('admin'),
    });
};

exports.down = function(knex) {

};
