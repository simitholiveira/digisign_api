
exports.up = function(knex) {
    return knex.schema.alterTable('playlist_slide', function(table) {
        table.dropColumn('duration');
    });
};

exports.down = function(knex) {
    return knex.schema.alterTable('playlist_slide', function(table) {
        table.integer('duration').unsigned().notNullable().after('slide_id');
    });
};
