
exports.up = function(knex) {
    return knex.schema.alterTable('event', function(table) {
        table.string('rec_pattern', 25).notNullable().defaultTo('').after('event_length');
        table.string('time', 25).notNullable().defaultTo('').after('rec_type');
        table.string('play_content', 1000).notNullable().defaultTo('').after('time');
    });
};

exports.down = function(knex) {
    return knex.schema.alterTable('event', function(table) {
        table.dropColumn('rec_pattern');
        table.dropColumn('time');
        table.dropColumn('play_content');
    });
};
