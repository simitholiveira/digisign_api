exports.up = function (knex) {
    return knex.schema.createTable('file', table => {
        table.increments('id').unsigned().notNullable();
        table.integer('slide_id').unsigned().notNullable();
        table.enum('type', ['html', 'media']).notNullable().defaultTo('html');
        table.string('name', 1000).notNullable();
        table.bigInteger('size').unsigned().notNullable();
        table.string('mime_type', 100).notNullable();
        table.timestamps(true, true);

        table.index('slide_id', 'slide_id');
        table.unique(['slide_id', 'name'], 'slide_file');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('file');
};
