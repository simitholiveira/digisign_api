
exports.up = function(knex) {
    return knex.schema.createTable('event', function(table) {
        table.bigInteger('id').unsigned().notNullable();
        table.integer('playlist_id').unsigned().notNullable();
        table.dateTime('start_date').notNullable();
        table.dateTime('end_date').notNullable();
        table.string('text', 50).notNullable().defaultTo('');
        table.bigInteger('event_pid').unsigned().notNullable().defaultTo(0);
        table.bigInteger('event_length').unsigned().notNullable();
        table.string('rec_type', 25).notNullable().defaultTo('');
        table.timestamps(true, true);

        table.primary(['id']);
        table.index(['playlist_id']);
        table.index(['event_pid']);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('event');
};
