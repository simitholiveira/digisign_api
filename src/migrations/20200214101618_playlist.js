
exports.up = function(knex) {
    return knex.schema.createTable('playlist', function(table) {
        table.increments('id').unsigned().notNullable();
        table.string('name', 50).notNullable();
        table.timestamps(true, true);

        table.unique(['name'], 'name');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('playlist');

};
