
exports.up = function(knex) {
    return knex.schema
        .raw('alter table `event` modify column `event_pid` bigint(20) unsigned DEFAULT NULL')
        .raw('alter table `event` modify column `event_length` bigint(20) unsigned DEFAULT NULL');
};

exports.down = function(knex) {
    return knex.schema
        .raw('alter table `event` modify column `event_pid` bigint(20) unsigned NOT NULL DEFAULT "0"')
        .raw('alter table `event` modify column `event_length` bigint(20) unsigned NOT NULL');
};
