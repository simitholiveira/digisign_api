const Knex = require('knex');
const { Model } = require('objection');

function db(config) {
    const knex = Knex(config);
    Model.knex(knex);
    return knex;
}

module.exports = {
    db,
};
