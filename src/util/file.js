const path = require('path');
const { inArray } = require('@utils');

function isHTMLByFileName(fileName) {
    const extension = (path.extname(fileName)).toLowerCase();
    return '.html' === extension;
}

function isImageByFileName(fileName) {
    const extension = (path.extname(fileName)).toLowerCase();
    return inArray(extension, ['.jpg', '.jpeg', '.png']);
}

function isVideoByFileName(fileName) {
    const extension = (path.extname(fileName)).toLowerCase();
    return inArray(extension, ['.mp4']);
}

function isHTMLByMimeType(mimeType) {
    return 'text/html' === mimeType;
}

function isImageByMimeType(mimeType) {
    return inArray(mimeType, ['image/jpg', 'image/jpeg', 'image/png']);
}

function isVideoByMimeType(mimeType) {
    return inArray(mimeType, ['video/mp4']);
}

module.exports = {
    isHTMLByFileName,
    isImageByFileName,
    isVideoByFileName,
    isHTMLByMimeType,
    isImageByMimeType,
    isVideoByMimeType,
};
