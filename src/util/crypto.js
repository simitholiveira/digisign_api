const crypto = require('crypto');
const uuid1 = require('uuid/v1');

const SALT_LENGTH = 32;
const ITERATIONS = 1000;
const KEY_LENGTH = 128;
const ALGO = 'sha512';

function getRandomString(length) {
    return crypto
        .randomBytes(Math.ceil(length / 2))
        .toString('hex')
        .slice(0,length);
}

function hashString(string, length) {
    return crypto
        .createHash(ALGO)
        .update(string)
        .digest('hex')
        .slice(0, length);
}

function hashPassword(password) {
    const salt = getRandomString(SALT_LENGTH);
    const hash =  crypto
        .pbkdf2Sync(password, salt, ITERATIONS, KEY_LENGTH, ALGO)
        .toString('hex');
    return salt + hash;
}

function validatePassword(password, passwordHash) {
    const salt = passwordHash.substr(0, SALT_LENGTH);
    const hash = passwordHash.substr(SALT_LENGTH);
    const check = crypto
        .pbkdf2Sync(password, salt, ITERATIONS, KEY_LENGTH, ALGO)
        .toString('hex');
    return hash === check;
}

function generateAccessToken() {
    return uuid1();
}


module.exports = {
    getRandomString,
    hashPassword,
    validatePassword,
    generateAccessToken,
    hashString
};
