const {isInt, isJson, inArray, isDefined} = require('@utils');
const SlideRepository = require('@repos/slide-repository');
const PlaylistRepository = require('@repos/playlist-repository');
const config = require('@config');
const { InvalidDataError } = require('@errors');


function isChildEvent(parentId) {
    return isInt(parentId) && 0 < parentId;
}

function isContentTypeDefined(contentType) {
    return isDefined(contentType.type) && isDefined(contentType.id);
}

function isContentTypeAllowed(contentType) {
    return inArray(contentType.type, config.event.allowedContentTypes);
}

async function assertEventContentTypeIsValid(contentType) {
    if (!isContentTypeDefined(contentType)) {
        throw new InvalidDataError;
    }
    if (!isContentTypeAllowed(contentType)) {
        throw new InvalidDataError;
    }
    if ('playlist' === contentType.type) {
        await PlaylistRepository.findByExternalId(contentType.id);
    } else if ('slide' === contentType.type) {
        await SlideRepository.findByExternalId(contentType.id);
    }
}

function isInputEventLengthValid(val) {
    return '' === val || (isInt(val) && 0 < val);
}

function sanitizeEmptyStringToNull(val) {
    return '' === val ? null : val;
}

function sanitizeNullToEmptyString(val) {
    return null === val ? '' : val;
}

function sanitizeJsonToString(val) {
    if (typeof(val) === 'object') {
        return JSON.stringify(val);
    }
    return val;
}

function sanitizeStringToJson(val) {
    if (isJson(val)) {
        return JSON.parse(val);
    }
    return val;
}

function sanitizeInputParentId(parentId) {
    return sanitizeEmptyStringToNull(parentId);
}

function sanitizeInputEventLength(val) {
    return sanitizeEmptyStringToNull(val);
}

function sanitizeOutputParentId(parentId) {
    return sanitizeNullToEmptyString(parentId);
}

function sanitizeOutputEventLength(val) {
    return sanitizeNullToEmptyString(val);
}

module.exports = {
    isChildEvent,
    isInputEventLengthValid,
    sanitizeInputParentId,
    sanitizeInputEventLength,
    sanitizeOutputParentId,
    sanitizeOutputEventLength,
    sanitizeJsonToString,
    sanitizeStringToJson,
    assertEventContentTypeIsValid
};
