const { InvalidDataError } = require('@errors');


function isEmpty(val) {
    if (Array.isArray(val)) {
        return !val.length;
    } else if ('object' === typeof(val) && null !== val) {
        return 0 === Object.keys(val).length;
    }
    return [undefined, null, ''].includes(val);
}

function isTrue(val) {
    return true === val || 'true' === val || Number.isInteger(val) && 0 !== val;
}

function inArray(val, arr) {
    return -1 !== arr.indexOf(val);
}

function isUndefined(val) {
    return typeof(val) === 'undefined';
}

function isDefined(val) {
    return !isUndefined(val);
}

function isFunction(val) {
    return typeof(val) === 'function';
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function isInt(val) {
    return parseInt(val, 10) === val;
}

function toInt(val) {
    val = parseInt(val);
    if (isNaN(val)) {
        throw new InvalidDataError('Unexpected recurrence value.');
    }
    return val;
}

module.exports = {
    isEmpty,
    isTrue,
    inArray,
    isDefined,
    isUndefined,
    isFunction,
    isInt,
    toInt,
    isJson,
};
