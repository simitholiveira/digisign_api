class Iterator {
    constructor(items) {
        this._index = 0;
        this._items = items;
    }
    next() {
        this._index++;
        return this;
    }
    valid() {
        return this._index < this._items.length;
    }
    key() {
        return this._index;
    }
    current(defaultValue = null) {
        if (this.valid()) {
            return this._items[this._index];
        }
        return defaultValue;
    }
    rewind() {
        this._index = 0;
        return this;
    }
    setKey(key) {
        this._index = key;
    }
    nextIfNotValidRewind() {
        this.next();
        if (!this.valid()) {
            this.rewind();
        }
    }
}

module.exports = Iterator;
