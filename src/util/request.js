
function filterFilesByFieldName(files, fieldName) {
    return files.filter(file => fieldName === file.fieldname);
}

module.exports = {
    filterFilesByFieldName,
};
