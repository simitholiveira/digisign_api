
function sortByProperty(name, direction = 'asc') {
    return function (a, b) {
        if (a[name] == b[name]) {
            return 0;
        }
        if ('asc' === direction) {
            return a[name] < b[name] ? -1 : 1;
        }
        return a[name] > b[name] ? -1 : 1;
    };
}

module.exports = {
    sortByProperty,
};
