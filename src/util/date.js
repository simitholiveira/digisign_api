const moment = require('moment');
/**
 * @param {Date} a
 * @param {Date} b
 */
function compareDates(a, b) {
    return a.getTime() === b.getTime();
}

/**
 * @param {Date|String} a
 * @returns {string}
 */
function dateToDbDate(a) {
    return moment(a).format('YYYY-MM-DD HH:mm');
}

function now(format) {
    return moment().format(format);
}

function laterForMinutes(minutes, format) {
    return moment().add(minutes, 'm').format(format);
}

function getWeekDayNumberByUnixSeconds(timestamp) {
    return moment(timestamp * 1000).local().day();
}

function getWeekNumberByUnixSeconds(timestamp) {
    return moment(timestamp * 1000).local().isoWeek();
}

function getMonthDayNumberByUnixSeconds(timestamp) {
    return moment(timestamp * 1000).local().format('DD');
}

function getMonthNumberByUnixSeconds(timestamp) {
    return moment(timestamp * 1000).local().month();
}

function getYearByUnixSeconds(timestamp) {
    return moment(timestamp * 1000).local().format('YYYY');
}

module.exports = {
    compareDates,
    dateToDbDate,
    now,
    laterForMinutes,
    getWeekDayNumberByUnixSeconds,
    getWeekNumberByUnixSeconds,
    getMonthDayNumberByUnixSeconds,
    getMonthNumberByUnixSeconds,
    getYearByUnixSeconds
};
