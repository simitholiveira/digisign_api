class EventPublisher {
    constructor() {
        this._eventSubscribes = {};
    }

    on(eventName, subscribedFunction) {
        this._verifyIfSubscriberIsFunction(subscribedFunction);
        this._subscribeFunctionOnEvent(subscribedFunction, eventName);
    }

    _verifyIfSubscriberIsFunction(subscriber) {
        if (typeof(subscriber) !== 'function') {
            throw new TypeError('Subscriber has to be a function.');
        }
    }

    _subscribeFunctionOnEvent(subscribedFunction, eventName) {
        if (!this._eventSubscribes.hasOwnProperty(eventName)) {
            this._eventSubscribes[eventName] = [];
        }
        this._eventSubscribes[eventName].push(subscribedFunction);
    }

    notify(eventName, data) {
        if (this._eventSubscribes.hasOwnProperty(eventName)) {
            for (let subscribedFunction of this._eventSubscribes[eventName]) {
                subscribedFunction(data);
            }
        }
    }
}

module.exports = EventPublisher;
