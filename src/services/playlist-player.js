const { isFunction, isUndefined } = require('@utils');
const { InvalidConfigurationError } = require('@errors');
const {SlideFacade} = require('@services/slide/slide-facade');
const Browser = require('@services/chrome-service');

class PlaylistPlayer {
    constructor(playlistSlidesIterator) {
        this._playlistSlidesIterator = playlistSlidesIterator;
    }

    setIsPlayingMustBeSuspendedFunction(func) {
        if (!isFunction(func)) {
            throw new TypeError('IsPlayingMustBeSuspended has to be a function.');
        }
        this._isPlayingMustBeSuspended = func;
    }

    setSuspendPlayingFunction(func) {
        if (!isFunction(func)) {
            throw new TypeError('SuspendPlaying has to be a function.');
        }
        this._suspendPlaying = func;
    }

    play() {
        this._verifyPlayerConfigurationCorrect();
        this._showPlaylistSlidesRecursively();
    }

    _verifyPlayerConfigurationCorrect() {
        if (isUndefined(this._isPlayingMustBeSuspended)) {
            throw new InvalidConfigurationError('IsPlayingSuspendedFunction has to be specified.');
        }
        if (isUndefined(this._suspendPlaying)) {
            throw new InvalidConfigurationError('SuspendPlaying has to be specified.');
        }
    }

    _showPlaylistSlidesRecursively() {
        const playlistSlide = this._playlistSlidesIterator.current();

        if (this._isPlayingMustBeSuspended(playlistSlide)) {
            this._suspendPlaying();
            return;
        }

        this._planShowingNextSlideInMilliseconds(playlistSlide.getDurationInMilliseconds());

        Browser.showSlide(playlistSlide.getSlide());
    }

    _planShowingNextSlideInMilliseconds(milliseconds) {
        setTimeout(
            () => {
                this._playlistSlidesIterator.nextIfNotValidRewind();
                this._showPlaylistSlidesRecursively();
            },
            milliseconds
        );
    }
}

module.exports = {
    PlaylistPlayer
};

