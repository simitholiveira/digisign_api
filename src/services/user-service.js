const {isEmpty} = require('../util');

class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }

    async generateAccessToken(user, regenerate = true) {
        if (regenerate || isEmpty(user.access_token)) {
            user.generateAccessToken();
            await this.userRepository.save(user);
        }
    }

    async changePassword(user, password) {
        user.password = password;
        await this.userRepository.save(user);
    }

    async resetAccessToken(user) {
        user.resetAccessToken();
        await this.userRepository.save(user);
    }
}

module.exports = UserService;
