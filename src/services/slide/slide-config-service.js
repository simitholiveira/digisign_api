const { isDefined } = require('@utils');


function setRequestDataToSlide(data, slide) {
    if (isDefined(data.durationInSeconds)) {
        slide.setDuration(data.durationInSeconds);
    }
    if (isDefined(data.name)) {
        slide.setName(data.name);
    }
    if (isDefined(data.thumbnail)) {
        slide.setThumbnail(data.thumbnail);
    }
    if (isDefined(data.highest_zIndex)) {
        slide.setHighestZIndex(data.highest_zIndex);
    }
    if (isDefined(data.assets)) {
        slide.setAssets(data.assets);
    }
    if (isDefined(data.css)) {
        slide.setCss(data.css);
    }
    if (isDefined(data.styles)) {
        slide.setStyles(data.styles);
    }
    if (isDefined(data.components)) {
        slide.setComponents(data.components);
    }
}

module.exports = {
    setRequestDataToSlide,
};
