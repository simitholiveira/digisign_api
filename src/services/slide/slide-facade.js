const {getSlideBaseUrl} = require('@repos/file-storage-repository');


class SlideFacade {
    constructor(slide) {
        this._createUrl(slide);
        this._durationInMilliseconds = slide.getMsSlideDuration();
        this._id = slide.getId();
    }

    _createUrl(slide) {
        const html = slide.newestHtml;
        const baseUrl = getSlideBaseUrl(slide);
        this._url = `${baseUrl}/${html.system_name}`;
    }

    getId() {
        return this._id;
    }

    getUrl() {
        return this._url;
    }

    getDurationInMilliseconds() {
        return this._durationInMilliseconds;
    }
}

module.exports = {
    SlideFacade
};
