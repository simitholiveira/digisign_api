const { isDefined } = require('@utils');
const { InvalidDataError } = require('@errors');
const {hashString, getRandomString} = require('@utils/crypto');


function createFileSystemName(file) {
    if ('html' === file.type) {
        return createHtmlSystemName(file);
    } else if ('media' === file.type) {
        return createMediaSystemName(file);
    }
    throw new InvalidDataError('Wrong type of file.');
}

function createHtmlSystemName(file) {
    const saltLength = 8;
    const hashLength = 8;
    const salt = getRandomString(saltLength);
    const postfix = hashString(salt, hashLength);
    return 'html-' + postfix + '.html';
}

function createMediaSystemName(file) {
    return file.name;
}

module.exports = {
    createFileSystemName
};
