const Slide = require('@models/slide-model');
const File = require('@models/file-model');
const { setRequestDataToSlide } = require('@services/slide/slide-config-service');

class SlideService {

    constructor(slideRepository) {
        this.slideRepository = slideRepository;
    }

    async createSlide(slideAttributes, htmlFile, mediaFiles = []) {
        const slide = new Slide;
        slide.setExternalId(slideAttributes.id);
        setRequestDataToSlide(slideAttributes, slide);
        slide.addFile(htmlFile);
        mediaFiles.forEach(file => slide.addFile(file));

        await this.slideRepository.save(slide);
    }

    async updateFiles(slide, files) {
        files.forEach(file => {
            const savedFile = slide.getFileByName(file.name);
            if (!savedFile) {
                slide.addFile(file);
            } else {
                slide.updateFile(file);
            }
        });
        await this.slideRepository.save(slide);
    }
}

function createFileFromUpload(upload) {
    let file = File.create(upload.originalname, upload.size, upload.mimetype);
    file.path = upload.path;
    return file;
}

module.exports = {
    SlideService,
    createFileFromUpload,
};
