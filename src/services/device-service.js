const {hashString} = require('@utils/crypto');
const config = require('@root/config');
// const macaddress = require('macaddress');
const sysInfo = require('systeminformation');

function isInterfaceInternal(interface) {
    return true === interface.internal;
}

async function getMainDeviceInterface() {
    let interfaces = await sysInfo.networkInterfaces();
    interfaces = Object.values(interfaces);
    for (let interface of interfaces) {
        if (!isInterfaceInternal(interface) && interface.ip4.length) {
            return interface;
        }
    }
    throw new Error('Main interface was not found.');
}

// async function getDeviceMacAddress() {
//     let deviceMacAddress = null;
//     await macaddress.one((err, mac) => {
//         deviceMacAddress = mac;
//     });
//     return deviceMacAddress;
// }

async function getDeviceMacAddress() {
    const interface = await getMainDeviceInterface();
    return interface.mac;
}

async function getDeviceIp4Address() {
    const interface = await getMainDeviceInterface();
    return interface.ip4;
}

async function getUniqueDeviceId() {
    const deviceMacAddress = await getDeviceMacAddress();
    const macAddressHash = hashString(deviceMacAddress, config.deviceId.length);
    return config.deviceId.prefix + macAddressHash;
}

module.exports = {
    getUniqueDeviceId,
    getDeviceIp4Address
};
