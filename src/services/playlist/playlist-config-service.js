const {hashString, getRandomString} = require('@utils/crypto');

const PLAYLIST_PREFIX = 'playlist-';

function generatePlaylistId() {
    const saltLength = 8;
    const hashLength = 12;
    const salt = getRandomString(saltLength);
    const postfix = hashString(salt, hashLength);
    return PLAYLIST_PREFIX + postfix;
}

function getPlaylistRequestBodyByParams(params) {
    const {playlistId, slideId} = params;
    return {
        body: {
            id: playlistId,
            slides: [{
                id: slideId
            }]
        }
    };
}

module.exports = {
    generatePlaylistId,
    getPlaylistRequestBodyByParams
};
