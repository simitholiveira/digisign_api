
class PlaylistSlideFacade {
    constructor(playlistSlide) {
        this._id = playlistSlide.getId();
    }

    setSlide(slide) {
        this._slideFacade = slide;
    }

    getSlide() {
        return this._slideFacade;
    }

    getId() {
        return this._id;
    }

    getUrl() {
        return this._slideFacade.getUrl();
    }

    getDurationInMilliseconds() {
        return this._slideFacade.getDurationInMilliseconds();
    }
}

module.exports = {
    PlaylistSlideFacade
};
