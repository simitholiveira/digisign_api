const Playlist = require('@models/playlist-model');

class PlaylistService {

    constructor(playlistRepository) {
        this.playlistRepository = playlistRepository;
    }

    async createPlaylist(playlistAttributes, playlistSlides) {
        const { id } = playlistAttributes;
        let playlist = Playlist.create(id);
        playlistSlides.forEach(playlistSlide => playlist.addPlaylistSlide(playlistSlide));
        await this.playlistRepository.save(playlist);
    }

    async updatePlaylist(playlist, playlistSlides) {
        playlistSlides.forEach(playlistSlide => playlist.addPlaylistSlide(playlistSlide));
        await this.playlistRepository.deletePlaylistSlidesByPlaylistId(playlist.getId());
        await this.playlistRepository.save(playlist);
    }
}

module.exports = PlaylistService;
