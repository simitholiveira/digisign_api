const puppeteer = require('puppeteer-core');
const config = require('@config');

var window = null;

class ChromeService {
    async launch(url) {
        return puppeteer.launch({
            headless: false,
            ignoreDefaultArgs: true,
            defaultViewport: null,
            executablePath: config.chrome.executablePath,
            args: [
                `--app=${url}`,
                '--autoplay-policy=no-user-gesture-required',
                '--kiosk',
            ],
        }).then(browser => {
            window = browser;
            if (browser) {
                browser.on('disconnected', onDisconnected);
            }
        });
    }

    async goTo(url) {
        if (!window) {
            return this.launch(url);
        }
        const pages = await window.pages();
        if (!pages.length) {
            return;
        }
        return pages[0].goto(url);
    }
}

function closeWindow() {
    if (window) {
        window.close();
    }
}

function showScreen(url) {
    const chrome = new ChromeService;
    chrome.goTo(url);
}

function showSlide(slideInterface) {
    showScreen(slideInterface.getUrl());
}

function showDefaultScreen() {
    showScreen(config.chrome.defaultUrl);
}

module.exports = {
    closeWindow,
    showSlide,
    showScreen,
    showDefaultScreen,
};

function onDisconnected() {
    window = null;
}
