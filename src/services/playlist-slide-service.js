const SlideRepository = require('@repos/slide-repository');
const PlaylistSlide = require('@models/playlist-slide-model');

async function createPlaylistSlideByExternalSlideId(slideId) {
    const model = await SlideRepository.findByExternalId(slideId);
    return PlaylistSlide.create(model.id);
}

module.exports = {
    createPlaylistSlideByExternalSlideId
};
