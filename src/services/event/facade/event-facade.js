
class EventFacade {
    constructor(event) {
        this._id = event.getId();
    }

    setPlaylistSlides(playlistSlides) {
        this._playlistSlides = playlistSlides;
    }

    getPlaylistSlides() {
        return this._playlistSlides;
    }

    getId() {
        return this._id;
    }
}

module.exports = {
    EventFacade
};
