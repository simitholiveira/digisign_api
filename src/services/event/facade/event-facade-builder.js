const {SlideFacade} = require('@services/slide/slide-facade');
const {PlaylistSlideFacade} = require('@services/playlist/playlist-slide-facade');
const {EventFacade} = require('@services/event/facade/event-facade');


class EventFacadeBuilder {

    static createSlideFacade(slide) {
        return new SlideFacade(slide);
    }

    static createPlaylistSlideFacade(playlistSlide) {
        const sFacade = this.createSlideFacade(playlistSlide.slide);
        const psFacade = new PlaylistSlideFacade(playlistSlide);
        psFacade.setSlide(sFacade);
        return psFacade;
    }

    static createEventFacade(event) {
        const psFacades = event.playlistSlides.map(item => this.createPlaylistSlideFacade(item));
        const eFacade = new EventFacade(event);
        eFacade.setPlaylistSlides(psFacades);
        return eFacade;
    }

}

module.exports = EventFacadeBuilder;
