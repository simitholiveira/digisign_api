const BaseRecTypeParser = require('./base-rec-type-parser');


class MockRecTypeParser extends BaseRecTypeParser {

    constructor(recType, recCount) {
        super();
        this._type = recType;
        this._count = recCount;
    }

    setWeekDays(weekDays) {
        this._weekDays = weekDays;
    }

    setCasesLimit(casesLimit) {
        this._casesLimit = casesLimit;
    }
}

module.exports = MockRecTypeParser;
