const { inArray, isEmpty, toInt, isInt } = require('@utils');
const { InvalidDataError } = require('@errors');
const config = require('@config');
const BaseRecTypeParser = require('./base-rec-type-parser');

const
    sundayNumber = 0,
    saturdayNumber = 6,
    firstWeek = 1,
    lastWeek = 4;

class RecTypeParser extends BaseRecTypeParser {

    constructor(recType) {
        super();
        this._parseRecType(recType);
        this._defineType();
        this._defineCount();
        this._defineMonthDays();
        this._defineWeekDays();
        this._defineCasesLimit();
    }

    _parseRecType(fullRecType) {
        const [recType, casesLimit] = fullRecType.split('#');
        const [type, count, monthDay, monthCount, weekDays] = recType.split('_');
        this._parsedRecType = {
            type: type || '',
            count: count || 0,
            monthDay: monthDay || '',
            monthCount: monthCount || '',
            weekDays: weekDays || [],
            casesLimit: casesLimit || ''
        };
    }

    _defineType() {
        const {type} = this._parsedRecType;
        if (!inArray(type, config.event.availableRecurrenceTypes)) {
            throw new InvalidDataError('Unexpected recurrence type.');
        }
        this._type = type;
    }

    _defineCount() {
        let {count} = this._parsedRecType;
        count = toInt(count);
        this._assertValInRange(count, {
            min: config.event.minRecurrenceCount,
            max: config.event.maxRecurrenceCount
        });
        this._count = count;
    }

    _defineMonthDays() {
        if (!this.isMonthlyRecurrenceType()) {
            return;
        }

        let {monthDay, monthCount} = this._parsedRecType;
        if ('' === monthDay && '' === monthCount) {
            return;
        }

        monthDay = toInt(monthDay);
        this._assertValInRange(monthDay, {min: sundayNumber, max: saturdayNumber});
        this._monthDay = monthDay;

        monthCount = toInt(monthCount);
        this._assertValInRange(monthCount, {min: firstWeek, max: lastWeek});
        this._monthCount = monthCount;
    }

    _defineWeekDays() {
        if (!this.isWeeklyRecurrenceType()) {
            return;
        }
        const {weekDays} = this._parsedRecType;
        if (isEmpty(weekDays)) {
            throw new InvalidDataError('Unexpected recurrence week days.');
        }
        const parsedWeekDays = weekDays.split(',');
        parsedWeekDays.map(dayNumber => {
            dayNumber = toInt(dayNumber);
            this._assertValInRange(dayNumber, {min: sundayNumber, max: saturdayNumber});
            this._weekDays.push(dayNumber);
        });
    }

    _defineCasesLimit() {
        let {casesLimit} = this._parsedRecType;
        if (inArray(casesLimit, ['', 'no'])) {
            this._casesLimit = casesLimit;
        } else {
            this._casesLimit = toInt(casesLimit);
        }
    }

    _assertValInRange(val, range) {
        const isOutOfRange = val > range.max || val < range.min;
        if (isOutOfRange) {
            throw new InvalidDataError('Unexpected recurrence days.');
        }
    }
}

module.exports = RecTypeParser;
