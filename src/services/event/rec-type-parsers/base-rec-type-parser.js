const { isInt } = require('@utils');

class BaseRecTypeParser {

    constructor() {
        this._type = '';
        this._count = 0;
        this._monthDay = '';
        this._monthCount = '';
        this._weekDays = [];
        this._casesLimit = '';
    }

    getType() {
        return this._type;
    }

    getCount() {
        return this._count;
    }

    getMonthDay() {
        return this._monthDay;
    }

    getMonthCount() {
        return this._monthCount;
    }

    getWeekDays() {
        return this._weekDays;
    }

    getCasesLimit() {
        return this._casesLimit;
    }

    isLimitedByNumber() {
        return isInt(this._casesLimit);
    }

    isDailyRecurrenceType() {
        return 'day' === this._type;
    }

    isWeeklyRecurrenceType() {
        return 'week' === this._type;
    }

    isMonthlyRecurrenceType() {
        return 'month' === this._type;
    }
}

module.exports = BaseRecTypeParser;
