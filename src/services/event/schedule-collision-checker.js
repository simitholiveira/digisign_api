
class ScheduleCollisionChecker {

    setFirstSchedule(schedule) {
        this._firstSchedule = schedule;
    }

    setSecondSchedule(schedule) {
        this._secondSchedule = schedule;
    }

    hasCollision() {
        for (let item of this._firstSchedule) {
            if (this._hasCollisionForStartEndDates(item.startAt, item.endAt)) {
                return true;
            }
        }
        return false;
    }

    _hasCollisionForStartEndDates(startAt, endAt) {
        for (let item of this._secondSchedule) {
            if (startAt <= item.endAt && endAt >= item.startAt) {
                return true;
            }
        }
        return false;
    }
}

module.exports = ScheduleCollisionChecker;
