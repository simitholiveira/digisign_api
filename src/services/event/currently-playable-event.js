class CurrentlyPlayableEvent {

    constructor() {
        this.freeUp();
    }

    setEvent(event) {
        this._event = event;
    }

    freeUp() {
        this._event = null;
    }

    isCurrentEvent(event) {
        return !this.isFree() && this._event.getId() === event.getId();
    }

    isFree() {
        return null === this._event;
    }
}

module.exports = CurrentlyPlayableEvent;
