const PlaylistRepository = require('@repos/playlist-repository');
const { InvalidDataError } = require('@errors');
const { createPlaylist } = require('@usecases/playlist/create-playlist');
const {
    generatePlaylistId,
    getPlaylistRequestBodyByParams
} = require('@services/playlist/playlist-config-service');


async function getPlaylistByContentTypeIfNotExistsCreate(contentType) {
    if ('playlist' === contentType.type) {
        return await PlaylistRepository.findByExternalId(contentType.id);
    }
    return createPlaylistByContentType(contentType);
}

function createPlaylistByContentType(contentType) {
    if ('slide' === contentType.type) {
        return createPlaylistForSlideId(contentType.id);
    }
    throw new InvalidDataError('Event content type is invalid.');
}

async function createPlaylistForSlideId(slideId) {
    const playlistId = generatePlaylistId();
    const params = getPlaylistRequestBodyByParams({playlistId, slideId});
    await createPlaylist(params);
    return await PlaylistRepository.findByExternalId(playlistId);
}

module.exports = {
    getPlaylistByContentTypeIfNotExistsCreate
};
