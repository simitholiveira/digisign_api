const { isDefined } = require('@utils');
const { sanitizeJsonToString } = require('@utils/event');


function setRequestDataToEvent(data, event) {
    if (isDefined(data.start_date)) {
        event.setStartAt(data.start_date);
    }
    if (isDefined(data.end_date)) {
        event.setEndAt(data.end_date);
    }
    if (isDefined(data.text)) {
        event.setText(data.text);
    }
    if (isDefined(data.event_length)) {
        event.setEventLength(data.event_length);
    }
    if (isDefined(data.event_pid)) {
        event.setEventParentId(data.event_pid);
    }
    if (isDefined(data.rec_type)) {
        event.setRecurrenceType(data.rec_type);
    }
    if (isDefined(data.rec_pattern)) {
        event.setRecurrencePattern(data.rec_pattern);
    }
    if (isDefined(data.time)) {
        event.setTime(data.time);
    }
    if (isDefined(data.play_content)) {
        event.setPlayContent(data.play_content);
    }
    if (isDefined(data.content_type)) {
        event.setContentType(sanitizeJsonToString(data.content_type));
    }
}

function isEventSchedulesEqual(firstEvent, secondEvent) {
    return String(firstEvent.getStartAt()) === String(secondEvent.getStartAt())
        && String(firstEvent.getEndAt()) === String(secondEvent.getEndAt())
        && firstEvent.getEventLength() === secondEvent.getEventLength()
        && firstEvent.getRecurrenceType() === secondEvent.getRecurrenceType();
}

function getEventDurationInMilliseconds(event) {
    if (event.getEventLength()) {
        return 1000 * event.getEventLength();
    }
    return 1000 * (event.getUnixEndAt() - event.getUnixStartAt());
}

module.exports = {
    setRequestDataToEvent,
    isEventSchedulesEqual,
    getEventDurationInMilliseconds
};
