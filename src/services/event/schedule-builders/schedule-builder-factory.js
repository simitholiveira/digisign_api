const DailyScheduleBuilder = require('@services/event/schedule-builders/daily-schedule-builder');
const WeeklyScheduleBuilder = require('@services/event/schedule-builders/weekly-schedule-builder');
const MonthlyScheduleBuilder = require('@services/event/schedule-builders/monthly-schedule-builder');
const OneTimeScheduleBuilder = require('@services/event/schedule-builders/one-time-schedule-builder');
const { InvalidDataError } = require('@errors');
const RecTypeParser = require('@services/event/rec-type-parsers/rec-type-parser');


class ScheduleBuilderFactory {

    createBuilderByEvent(event) {
        if (event.getRecurrenceType()) {
            return this._createBuilderByRecurrenceType(event.getRecurrenceType());
        }
        return this._createBuilderForOneTimeEvent();
    }

    _createBuilderByRecurrenceType(recType) {
        const recTypeParser = new RecTypeParser(recType);
        if (recTypeParser.isDailyRecurrenceType()) {
            return new DailyScheduleBuilder(recTypeParser);
        } else if (recTypeParser.isWeeklyRecurrenceType()) {
            return new WeeklyScheduleBuilder(recTypeParser);
        } else if (recTypeParser.isMonthlyRecurrenceType()) {
            return new MonthlyScheduleBuilder(recTypeParser);
        }
        throw new InvalidDataError('Unexpected repetition type.');
    }

    _createBuilderForOneTimeEvent() {
        return new OneTimeScheduleBuilder;
    }

}

module.exports = ScheduleBuilderFactory;
