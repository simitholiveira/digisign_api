
class EventScheduleBuilderWrapper {

    setScheduleBuilderFactory(scheduleBuilderFactory) {
        this._scheduleBuilderFactory = scheduleBuilderFactory;
    }

    setEvent(event) {
        this._event = event;
    }

    getSchedule() {
        const scheduleBuilder = this._getScheduleBuilder();
        return scheduleBuilder.createSchedule();
    }

    _getScheduleBuilder() {

        const scheduleBuilder = this._scheduleBuilderFactory.createBuilderByEvent(this._event);
        scheduleBuilder.setStartEndUnixTimestamps(
            this._event.getUnixStartAt(),
            this._event.getUnixEndAt()
        );
        scheduleBuilder.setEventLength(this._event.getEventLength());

        return scheduleBuilder;
    }
}

module.exports = EventScheduleBuilderWrapper;
