class DailyScheduleBuilder {

    constructor(recTypeParser) {
        this._recTypeParser = recTypeParser;
    }

    setStartEndUnixTimestamps(unixStartAt, unixEndAt) {
        this._unixStartAt = unixStartAt;
        this._unixEndAt = unixEndAt;
    }

    setEventLength(eventLength) {
        this._eventLength = eventLength;
    }

    createSchedule() {
        this._schedule = [];
        this._defineEventPeriodicRepetitionInSeconds();
        this._createSchedule();
        this._limitCases();
        return this._schedule;
    }

    _defineEventPeriodicRepetitionInSeconds() {
        const daySeconds = 86400;
        const periodicRepetitionInDays = this._recTypeParser.getCount();
        this._eventPeriodicRepetitionInSeconds = daySeconds * periodicRepetitionInDays;
    }

    _createSchedule() {

        let currentUnixStartAt = this._unixStartAt;
        let currentUnixEndAt = currentUnixStartAt + this._eventLength;

        while (this._isTimestampInDatesRange(currentUnixEndAt)) {

            this._appendStartEndTimestampsToSchedule(currentUnixStartAt, currentUnixEndAt);

            currentUnixStartAt += this._eventPeriodicRepetitionInSeconds;
            currentUnixEndAt = currentUnixStartAt + this._eventLength;
        }
    }

    _isTimestampInDatesRange(timestamp) {
        return this._unixEndAt >= timestamp && this._unixStartAt <= timestamp;
    }

    _appendStartEndTimestampsToSchedule(unixStartAt, unixEndAt) {
        this._schedule.push({
            startAt: unixStartAt,
            endAt: unixEndAt,
        });
    }

    _limitCases() {
        if (!this._recTypeParser.isLimitedByNumber()) {
            return;
        }
        const casesNumber = this._recTypeParser.getCasesLimit();
        this._schedule = this._schedule.slice(0, casesNumber);
    }
}

module.exports = DailyScheduleBuilder;
