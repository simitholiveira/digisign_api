class OneTimeScheduleBuilder {

    setStartEndUnixTimestamps(unixStartAt, unixEndAt) {
        this._unixStartAt = unixStartAt;
        this._unixEndAt = unixEndAt;
    }

    setEventLength() {
        // Once event lasts from start date to end date.
    }

    createSchedule() {
        return [{
            startAt: this._unixStartAt,
            endAt: this._unixEndAt,
        }];
    }
}

module.exports = OneTimeScheduleBuilder;
