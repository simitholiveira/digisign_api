const {inArray} = require('@utils');
const {
    getWeekDayNumberByUnixSeconds,
    getWeekNumberByUnixSeconds,
    getYearByUnixSeconds
} = require('@utils/date');


class WeeklyScheduleBuilder {

    constructor(recTypeParser) {
        this._recTypeParser = recTypeParser;
    }

    setStartEndUnixTimestamps(unixStartAt, unixEndAt) {
        this._unixStartAt = unixStartAt;
        this._unixEndAt = unixEndAt;
    }

    setEventLength(eventLength) {
        this._eventLength = eventLength;
    }

    createSchedule() {
        this._schedule = [];
        this._createSchedule();
        return this._schedule;
    }

    _createSchedule() {
        this._weeklySchedule = {};
        this._createWeeklySchedule();
        this._eliminateRedundantWeeks();
        this._limitCases();
    }

    _createWeeklySchedule() {
        const daySeconds = 86400;
        let currentUnixStartAt = this._unixStartAt;
        let currentUnixEndAt = currentUnixStartAt + this._eventLength;

        while (this._isTimestampInDatesRange(currentUnixEndAt)) {

            if (this._isTimestampSetToChosenWeekDay(currentUnixStartAt)) {
                this._appendStartEndTimestampsToWeeklySchedule(currentUnixStartAt, currentUnixEndAt);
            }

            currentUnixStartAt += daySeconds;
            currentUnixEndAt = currentUnixStartAt + this._eventLength;
        }
    }

    _isTimestampInDatesRange(timestamp) {
        return this._unixEndAt >= timestamp && this._unixStartAt <= timestamp;
    }

    _isTimestampSetToChosenWeekDay(timestamp) {
        const weekDayNumber = getWeekDayNumberByUnixSeconds(timestamp);
        return inArray(weekDayNumber, this._recTypeParser.getWeekDays());
    }

    _appendStartEndTimestampsToWeeklySchedule(unixStartAt, unixEndAt) {
        const weekKey = this._createWeekKeyByUnixSeconds(unixStartAt);
        if (!this._weeklySchedule.hasOwnProperty(weekKey)) {
            this._weeklySchedule[weekKey] = [];
        }
        this._weeklySchedule[weekKey].push({
            startAt: unixStartAt,
            endAt: unixEndAt,
        });
    }

    _createWeekKeyByUnixSeconds(unixStartAt) {
        const weekNumber = getWeekNumberByUnixSeconds(unixStartAt);
        const year = getYearByUnixSeconds(unixStartAt);
        return year + '-' + weekNumber;
    }

    _eliminateRedundantWeeks() {
        const weeklySchedule = Object.values(this._weeklySchedule);
        Object.keys(weeklySchedule).map(weekNumber => {
            if (0 === (weekNumber % this._recTypeParser.getCount())) {
                weeklySchedule[weekNumber].map(schedule => this._schedule.push(schedule));
            }
        });
    }

    _limitCases() {
        if (!this._recTypeParser.isLimitedByNumber()) {
            return;
        }
        const casesNumber = this._recTypeParser.getCasesLimit();
        this._schedule = this._schedule.slice(0, casesNumber);
    }
}

module.exports = WeeklyScheduleBuilder;
