const {
    getMonthDayNumberByUnixSeconds,
    getMonthNumberByUnixSeconds,
    getYearByUnixSeconds
} = require('@utils/date');


class MonthlyScheduleBuilder {

    constructor(recTypeParser) {
        this._recTypeParser = recTypeParser;
    }

    setStartEndUnixTimestamps(unixStartAt, unixEndAt) {
        this._unixStartAt = unixStartAt;
        this._unixEndAt = unixEndAt;
    }

    setEventLength(eventLength) {
        this._eventLength = eventLength;
    }

    createSchedule() {
        this._schedule = [];
        this._createSchedule();
        return this._schedule;
    }

    _createSchedule() {
        this._monthlySchedule = {};
        this._defineMonthDayNumberOfLaunching();
        this._createMonthlySchedule();
        this._eliminateRedundantMonths();
        this._limitCases();
    }

    _defineMonthDayNumberOfLaunching() {
        this._monthDayNumberOfLaunching = getMonthDayNumberByUnixSeconds(this._unixStartAt);
    }

    _createMonthlySchedule() {
        const daySeconds = 86400;
        let currentUnixStartAt = this._unixStartAt;
        let currentUnixEndAt = currentUnixStartAt + this._eventLength;

        while (this._isTimestampInDatesRange(currentUnixEndAt)) {

            if (this._isTimestampSetToChosenMonthDay(currentUnixStartAt)) {
                this._appendStartEndTimestampsToMonthlySchedule(currentUnixStartAt, currentUnixEndAt);
            }

            currentUnixStartAt += daySeconds;
            currentUnixEndAt = currentUnixStartAt + this._eventLength;
        }
    }

    _isTimestampInDatesRange(timestamp) {
        return this._unixEndAt >= timestamp && this._unixStartAt <= timestamp;
    }

    _isTimestampSetToChosenMonthDay(timestamp) {
        const monthDayNumber = getMonthDayNumberByUnixSeconds(timestamp);
        return monthDayNumber === this._monthDayNumberOfLaunching;
    }

    _appendStartEndTimestampsToMonthlySchedule(unixStartAt, unixEndAt) {
        const monthKey = this._createMonthKeyByUnixSeconds(unixStartAt);
        if (!this._monthlySchedule.hasOwnProperty(monthKey)) {
            this._monthlySchedule[monthKey] = [];
        }
        this._monthlySchedule[monthKey].push({
            startAt: unixStartAt,
            endAt: unixEndAt,
        });
    }

    _createMonthKeyByUnixSeconds(unixStartAt) {
        const monthNumber = getMonthNumberByUnixSeconds(unixStartAt);
        const year = getYearByUnixSeconds(unixStartAt);
        return year + '-' + monthNumber;
    }

    _eliminateRedundantMonths() {
        const monthlySchedule = Object.values(this._monthlySchedule);
        Object.keys(monthlySchedule).map(monthNumber => {
            if (0 === (monthNumber % this._recTypeParser.getCount())) {
                monthlySchedule[monthNumber].map(schedule => this._schedule.push(schedule));
            }
        });
    }

    _limitCases() {
        if (!this._recTypeParser.isLimitedByNumber()) {
            return;
        }
        const casesNumber = this._recTypeParser.getCasesLimit();
        this._schedule = this._schedule.slice(0, casesNumber);
    }
}

module.exports = MonthlyScheduleBuilder;
