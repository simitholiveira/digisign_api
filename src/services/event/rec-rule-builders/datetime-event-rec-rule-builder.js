const moment = require('moment');
const EventScheduleBuilderWrapper = require('@services/event/schedule-builders/event-schedule-builder-wrapper');
const ScheduleBuilderFactory = require('@services/event/schedule-builders/schedule-builder-factory');


class DatetimeEventRecRuleBuilder {

    constructor(event) {
        this._rules = [];
        this._event = event;
    }

    createRules() {
        this._createSchedule();
        this._prepareRulesBySchedule();
    }

    _createSchedule() {
        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        eventScheduleBuilder.setEvent(this._event);
        this._schedule = eventScheduleBuilder.getSchedule();
    }

    _prepareRulesBySchedule() {
        this._schedule.map(item => {
            const milliseconds = item.startAt * 1000;
            this._rules.push(moment(milliseconds).local().format('YYYY-MM-DD HH:mm:ss'));
        });
    }

    getRules() {
        return [...this._rules];
    }
}

module.exports = DatetimeEventRecRuleBuilder;
