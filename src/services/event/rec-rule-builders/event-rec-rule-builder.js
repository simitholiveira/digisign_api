const OneTimeEventRecRuleBuilder = require('@services/event/rec-rule-builders/one-time-event-rec-rule-builder');
const DailyEventRecRuleBuilder = require('@services/event/rec-rule-builders/daily-event-rec-rule-builder');
const WeeklyEventRecRuleBuilder = require('@services/event/rec-rule-builders/weekly-event-rec-rule-builder');
const DatetimeEventRecRuleBuilder = require('@services/event/rec-rule-builders/datetime-event-rec-rule-builder');
const { InvalidDataError } = require('@errors');
const RecTypeParser = require('@services/event/rec-type-parsers/rec-type-parser');


class EventRecRuleBuilderFactory {

    constructor(event) {
        this._event = event;
    }

    createBuilder() {
        if (this._event.getRecurrenceType()) {
            return this._createBuilderInDependenceOnRecType();
        }
        return this._createBuilderForOneTimeEvent();
    }

    _createBuilderInDependenceOnRecType() {
        const recTypeParser = new RecTypeParser(this._event.getRecurrenceType());
        if (recTypeParser.isDailyRecurrenceType()) {
            return new DailyEventRecRuleBuilder(this._event);
        } else if (recTypeParser.isWeeklyRecurrenceType()) {
            return new WeeklyEventRecRuleBuilder(this._event);
        }
        throw new InvalidDataError('Unexpected repetition type.');
    }

    _createBuilderForOneTimeEvent() {
        return new OneTimeEventRecRuleBuilder(this._event);
    }
}

function createEventRecurrenceRules(event) {
    const ruleBuilder = new DatetimeEventRecRuleBuilder(event);
    ruleBuilder.createRules();

    return ruleBuilder.getRules();
}

module.exports = {
    createEventRecurrenceRules
};
