const moment = require('moment');
const RecTypeParser = require('@services/event/rec-type-parsers/rec-type-parser');


class DailyEventRecRuleBuilder {

    constructor(event) {
        this._rule = {
            second: '0',
            minute: '*',
            hour: '*',
            dayOfMonth: '*',
            month: '*',
            dayOfWeek: '?',
        };
        this._parser = new RecTypeParser(event.getRecurrenceType());
        this._event = event;
    }

    createRules() {
        this._createDayOfMonthRule();
        this._createTimeRule(this._event.getStartAt());
    }

    _createDayOfMonthRule() {
        this._rule.dayOfMonth = '*/' + this._parser.getCount();
    }

    _createTimeRule(time) {
        const dateTime = moment(time).local();
        this._rule.hour = dateTime.format('HH');
        this._rule.minute = dateTime.format('mm');
        this._rule.second = dateTime.format('ss');
    }

    getRules() {
        const rule = [
            this._rule.second,
            this._rule.minute,
            this._rule.hour,
            this._rule.dayOfMonth,
            this._rule.month,
            this._rule.dayOfWeek,
        ];
        return [rule.join(' ')];
    }
}

module.exports = DailyEventRecRuleBuilder;
