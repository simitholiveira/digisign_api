const { Model } = require('objection');

const Slide = require('./slide-model');
const Playlist = require('./playlist-model');

/**
 * @property {number} id
 * @property {number} playlist_id
 * @property {number} slide_id
 *
 * @property {Slide} slide
 */
class PlaylistSlide extends Model {
    static create(slideId) {
        let model = new PlaylistSlide;
        model.playlist_id = null;
        model.slide_id = slideId;
        return model;
    }

    static get tableName() {
        return 'playlist_slide';
    }

    getId() {
        return this.id;
    }

    getSlideId() {
        return this.slide_id;
    }

    static get relationMappings() {
        return {
            slide: {
                relation: Model.BelongsToOneRelation,
                modelClass: Slide,
                join: {
                    from: 'playlist_slide.slide_id',
                    to: 'slide.id',
                },
            },
            playlist: {
                relation: Model.BelongsToOneRelation,
                modelClass: Playlist,
                join: {
                    from: 'playlist_slide.playlist_id',
                    to: 'playlist.id',
                },
            },
        };
    }
}

module.exports = PlaylistSlide;
