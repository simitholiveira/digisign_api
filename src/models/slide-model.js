const { Model } = require('objection');
const { sortByProperty } = require('@utils/sorters');
const File = require('./file-model');

/**
 * @property {number} id
 * @property {String} external_id
 * @property {String} name
 * @property {String} thumbnail
 * @property {String} assets
 * @property {String} styles
 * @property {String} components
 * @property {String} css
 * @property {number} duration
 * @property {number} highest_z_index
 * @property {File[]} files
 *
 * @property {File} html
 * @property {File[]} media
 */
class Slide extends Model {
    constructor() {
        super();
        this.id = null;
        this.external_id = null;
        this.duration = null;
        this.name = '';
        this.thumbnail = '';
        this.assets = '';
        this.styles = '';
        this.components = '';
        this.css = '';
        this.highest_z_index = 0;
        this.files = [];
    }

    getId() {
        return this.id;
    }

    setName(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    setThumbnail(thumbnail) {
        this.thumbnail = thumbnail;
    }

    getThumbnail() {
        return this.thumbnail;
    }

    setHighestZIndex(zIndex) {
        this.highest_z_index = zIndex;
    }

    getHighestZIndex() {
        return this.highest_z_index;
    }

    setStyles(styles) {
        this.styles = styles;
    }

    getStyles() {
        return this.styles;
    }

    setComponents(components) {
        this.components = components;
    }

    getComponents() {
        return this.components;
    }

    setAssets(assets) {
        this.assets = assets;
    }

    getAssets() {
        return this.assets;
    }

    setCss(css) {
        this.css = css;
    }

    getCss() {
        return this.css;
    }

    setExternalId(id) {
        this.external_id = id;
    }

    getExternalId() {
        return this.external_id;
    }

    setDuration(duration) {
        this.duration = duration;
    }

    getDuration() {
        return this.duration;
    }

    getMsSlideDuration() {
        return this.getDuration() * 1000;
    }

    // static create(name, htmlFile) {
    //     let model = new Slide;
    //     model.id = null;
    //     model.name = name;
    //     model.files = [htmlFile];
    //     return model;
    // }

    addFile(file) {
        this.files.push(file);
    }

    updateFile(file) {
        let savedFile = this.files.findIndex(f => file.name === f.name);
        if (-1 === savedFile) {
            throw new Error(`File ${file.name} not found`);
        }
        this.files[savedFile].updateFile(file);
    }

    get htmls() {
        return this.files.filter(f => File.HTML === f.type);
    }

    get newestHtml() {
        let htmls = this.htmls.sort(sortByProperty('id', 'desc'));
        return htmls[0];
    }

    get media() {
        return this.files.filter(f => File.MEDIA === f.type);
    }

    getFileByName(name) {
        return this.files.find(f => name === f.name);
    }

    getSlideDirName() {
        return this.external_id;
    }

    static get tableName() {
        return 'slide';
    }

    static get relationMappings() {
        return {
            files: {
                relation: Model.HasManyRelation,
                modelClass: File,
                join: {
                    from: 'slide.id',
                    to: 'file.slide_id',
                },
            },
        };
    }

    static get attributes() {
        return [
            'id',
            'external_id',
            'duration',
            'highest_z_index',
            'name',
            'thumbnail',
            'css',
            'assets',
            'styles',
            'components',
        ];
    }
}

module.exports = Slide;
