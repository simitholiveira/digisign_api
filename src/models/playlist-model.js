const { Model } = require('objection');

const PlaylistSlide = require('./playlist-slide-model');

/**
 * @property {number} id
 * @property {string} external_id
 * @property {PlaylistSlide[]} playlistSlides
 */
class Playlist extends Model {
    constructor() {
        super();
        this.playlistSlides = [];
    }

    static create(externalId) {
        let model = new Playlist;
        model.external_id = externalId;
        return model;
    }

    addPlaylistSlide(playlistSlide) {
        this.playlistSlides.push(playlistSlide);
    }

    static get tableName() {
        return 'playlist';
    }

    getId() {
        return this.id;
    }

    getExternalId() {
        return this.external_id;
    }

    static get relationMappings() {
        return {
            playlistSlides: {
                relation: Model.HasManyRelation,
                modelClass: PlaylistSlide,
                join: {
                    from: 'playlist.id',
                    to: 'playlist_slide.playlist_id',
                },
            },
        };
    }
}

module.exports = Playlist;
