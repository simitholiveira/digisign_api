const { Model } = require('objection');
const moment = require('moment');
const { sanitizeStringToJson } = require('@utils/event');
const Playlist = require('./playlist-model');
const PlaylistSlide = require('./playlist-slide-model');
const Slide = require('./slide-model');

/**
 * @property {Number} id
 * @property {Number} playlist_id
 * @property {Date} start_date
 * @property {Date} end_date
 * @property {String} text
 * @property {Number} event_length
 * @property {Number} event_pid
 * @property {String} rec_type
 * @property {String} rec_pattern
 * @property {String} time
 * @property {String} play_content
 * @property {JSON} content_type
 */
class Event extends Model {

    static get tableName() {
        return 'event';
    }

    setId(id) {
        this.id = id;
    }

    getId() {
        return this.id;
    }

    setPlaylist(playlist) {
        this.playlist_id = playlist.id;
    }

    getPlaylistId() {
        return this.playlist_id;
    }

    setStartAt(startDate) {
        this.start_date = moment(startDate).toDate();
    }

    getStartAt() {
        return this.start_date;
    }

    getUnixStartAt() {
        return moment(this.start_date)
            .milliseconds(0)
            .format('x') / 1000;
    }

    setEndAt(endDate) {
        this.end_date = moment(endDate).toDate();
    }

    getEndAt() {
        return this.end_date;
    }

    getUnixEndAt() {
        return moment(this.end_date)
            .milliseconds(0)
            .format('x') / 1000;
    }

    setText(text) {
        this.text = text;
    }

    getText() {
        return this.text;
    }

    setEventLength(eventLength) {
        this.event_length = eventLength;
    }

    getEventLength() {
        return this.event_length;
    }

    setEventParentId(pid) {
        this.event_pid = pid;
    }

    getEventParentId() {
        return this.event_pid;
    }

    setRecurrenceType(recType) {
        this.rec_type = recType;
    }

    getRecurrenceType() {
        return this.rec_type;
    }

    setRecurrencePattern(pattern) {
        this.rec_pattern = pattern;
    }

    getRecurrencePattern() {
        return this.rec_pattern;
    }

    setTime(time) {
        this.time = time;
    }

    getTime() {
        return this.time;
    }

    setPlayContent(playContent) {
        this.play_content = playContent;
    }

    getPlayContent() {
        return this.play_content;
    }

    setContentType(contentType) {
        this.content_type = contentType;
    }

    getContentType() {
        return this.content_type;
    }

    getContentTypeAsJson() {
        return sanitizeStringToJson(this.content_type);
    }

    static get relationMappings() {
        return {
            playlist: {
                relation: Model.BelongsToOneRelation,
                modelClass: Playlist,
                join: {
                    from: 'event.playlist_id',
                    to: 'playlist.id',
                },
            },
            playlistSlides: {
                relation: Model.HasManyRelation,
                modelClass: PlaylistSlide,
                join: {
                    from: 'event.playlist_id',
                    to: 'playlist_slide.playlist_id',
                },
            },
            slides: {
                relation: Model.ManyToManyRelation,
                modelClass: Slide,
                join: {
                    from: 'event.playlist_id',
                    through: {
                        from: 'playlist_slide.playlist_id',
                        to: 'playlist_slide.slide_id',
                    },
                    to: 'slide.id',
                },
            },
        };
    }
}

module.exports = Event;
