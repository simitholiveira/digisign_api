const { Model } = require('objection');
const {
    hashPassword,
    validatePassword,
    generateAccessToken
} = require('../util/crypto');

/**
 * @param {Number} id
 * @param {String} username
 * @param {String} password_hash
 * @param {String} password_salt
 */
class User extends Model {
    static create(username) {
        let model = new User();
        model.id = null;
        model.username = username;
        model.password_hash = null;
        model.access_token = null;
        return model;
    }

    set password(val) {
        this.password_hash = hashPassword(val);
    }

    validatePassword(val) {
        return validatePassword(val, this.password_hash);
    }

    generateAccessToken() {
        this.access_token = generateAccessToken();
    }

    resetAccessToken() {
        this.access_token = null;
    }

    static get tableName() {
        return 'user';
    }
}

module.exports = User;
