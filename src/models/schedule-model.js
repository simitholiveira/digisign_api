const { Model } = require('objection');
const moment = require('moment');

const Slide = require('./slide-model');

/**
 * @property {Number} id
 * @property {Number} slide_id
 * @property {String} start_at
 * @property {String} stop_at
 */
class Schedule extends Model {
    constructor(startAt, stopAt) {
        super();
        this.id = null;
        this.slide_id = null;
        this.start_at = startAt;
        this.stop_at = stopAt;
    }

    /**
     * @returns {null|Date}
     */
    get startDate() {
        if (!this.start_at) {
            return null;
        }
        return moment(this.start_at).toDate();
    }

    /**
     * @returns {null|Date}
     */
    get stopDate() {
        if (!this.stop_at) {
            return null;
        }
        return moment(this.stop_at).toDate();
    }

    static get tableName() {
        return 'schedule';
    }

    static get relationMappings() {
        return {
            slide: {
                relation: Model.BelongsToOneRelation,
                modelClass: Slide,
                join: {
                    from: 'slide_id',
                    to: 'slide.id',
                },
            },
        };
    }
}

module.exports = Schedule;
