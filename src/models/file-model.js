const pick = require('lodash/pick');
const { Model } = require('objection');
const { isHTMLByMimeType } = require('@utils/file');
const { createFileSystemName } = require('@services/slide/file-config-service');

/**
 * @property {Number} id
 * @property {Number} slide_id
 * @property {String} name
 * @property {String} mime_type
 * @property {Number} size
 * @property {String} type
 */
class File extends Model {
    static create(name, size, mimeType) {
        let model = new File;
        model.name = name;
        model.mime_type = mimeType;
        model.size = size;
        model.type = isHTMLByMimeType(mimeType) ? File.HTML : File.MEDIA;

        model.system_name = createFileSystemName(model);

        return model;
    }

    updateFile(file) {
        const update = pick(file, [
            'name', 'system_name', 'path', 'mime_type', 'size', 'type',
        ]);
        Object.assign(this, update);
    }

    static get tableName() {
        return 'file';
    }

    static get attributes() {
        return [
            'id',
            'slide_id',
            'name',
            'system_name',
            'mime_type',
            'size',
            'type',
        ];
    }
}

File.HTML = 'html';
File.MEDIA = 'media';

module.exports = File;
