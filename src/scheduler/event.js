require('module-alias/register');
const config = require('@config');
const db = require('knex')(config.db);
const { Model } = require('objection');
const { createEventRecurrenceRules } = require('@services/event/rec-rule-builders/event-rec-rule-builder');
const scheduler = require('node-schedule');
const Browser = require('@services/chrome-service');
const JobStore = require('./job-store');
const {getUpcomingEvents} = require('./upcoming-events');
const {
    isEventSchedulesEqual,
    getEventDurationInMilliseconds
} = require('@services/event/event-config-service');
const eventPublisher = require('./event-publisher');
const Iterator = require('@utils/iterator');
const CurrentlyPlayableEvent = require('@services/event/currently-playable-event');
const { PlaylistPlayer } = require('@services/playlist-player');
const EventFacadeBuilder = require('@services/event/facade/event-facade-builder');

Model.knex(db);

const eventJobStore = new JobStore;

let currentlyPlayableEvent = new CurrentlyPlayableEvent;

scanEventsForScheduling();
const scanner = setInterval(scanEventsForScheduling, config.scheduler.scanInterval);
let timeout;

process.on('exit', () => {
    db.destroy();
    Browser.closeWindow();
    clearInterval(scanner);
    clearTimeout(timeout);
    eventJobStore.forEach(eventJob => {
        cancelEventJobInScheduler(eventJob);
    });
});

async function scanEventsForScheduling() {
    eventPublisher.notify('beforeScanEvents');
    const events = await getUpcomingEvents();
    events.forEach(event => {
        processEventSchedule(event);
    });
}

function processEventSchedule(event) {
    try {
        scheduleEvent(event);
    } catch (e) {
        eventPublisher.notify('error', e);
    }
}

function scheduleEvent(event) {
    verifyIfEventHasToBeScheduled(event);
    eventPublisher.notify('beforeScheduleEvent', {event});
    cancelEventIfScheduled(event);

    const openJobs = scheduleEventRecurrenceJobs(event);
    const closeJob = scheduleEventTerminateJob(event);

    eventJobStore.set(event.getId(), {openJobs, closeJob, event});
}

function verifyIfEventHasToBeScheduled(event) {
    const eventJob = eventJobStore.get(event.getId());
    if (eventJob && isEventSchedulesEqual(eventJob.event, event)) {
        throw new Error(`SCHEDULED EVENT #${event.getId()}`);
    }
}

function scheduleEventRecurrenceJobs(event) {
    const rules = createEventRecurrenceRules(event);
    eventPublisher.notify('afterCreateEventRecurrenceRules', {event, rules});
    const eventFacade = EventFacadeBuilder.createEventFacade(event);
    return rules.map(rule => {
        return scheduler.scheduleJob(rule, () => {
            currentlyPlayableEvent.setEvent(event);
            setEventSuspendingTimer(event);
            runEvent(eventFacade);
        });
    });
}

function scheduleEventTerminateJob(event) {
    return scheduler.scheduleJob(event.getEndAt(), () => {
        eventPublisher.notify('beforeTerminateEvent', {event});
        cancelEventIfScheduled(event);
        terminateEvent(event);
        if (currentlyPlayableEvent.isCurrentEvent(event)) {
            currentlyPlayableEvent.freeUp();
            Browser.showDefaultScreen();
        }
    });
}

function setEventSuspendingTimer(event) {
    const duration = getEventDurationInMilliseconds(event);
    eventPublisher.notify('beforeSettingEventSuspendingTimer', {event, duration});
    timeout = setTimeout(
        () => {
            eventPublisher.notify('beforeSuspendEvent', {event});
            currentlyPlayableEvent.freeUp();
            Browser.showDefaultScreen();
        },
        duration
    );
}

function runEvent(eventFacade) {
    eventPublisher.notify('beforeRunEvent', {event: eventFacade});
    const iterator = new Iterator(eventFacade.getPlaylistSlides());
    const player = new PlaylistPlayer(iterator);
    player.setIsPlayingMustBeSuspendedFunction((playlistSlide) => {
        const isSuspended = !currentlyPlayableEvent.isCurrentEvent(eventFacade);
        eventPublisher.notify('beforeShowSlide', {playlistSlide, isSuspended});
        return isSuspended;
    });
    player.setSuspendPlayingFunction(() => ({}));
    player.play();
}

function cancelEventIfScheduled(event) {
    const eventJob = eventJobStore.get(event.getId());
    if (eventJob) {
        eventPublisher.notify('beforeCancelEvent', {event});
        cancelEventJobInScheduler(eventJob);
    }
}

function cancelEventJobInScheduler(eventJob) {
    if (eventJob.openJobs) {
        eventJob.openJobs.map(openJob => {
            scheduler.cancelJob(openJob);
        });
    }
    if (eventJob.closeJob) {
        scheduler.cancelJob(eventJob.closeJob);
    }
}

function terminateEvent(event) {
    eventJobStore.delete(event.getId());
    eventPublisher.notify('afterTerminateEvent', {event, eventJobStore});
}

