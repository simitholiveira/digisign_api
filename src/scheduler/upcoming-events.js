require('module-alias/register');
const config = require('@config');
const db = require('knex')(config.db);
const { Model } = require('objection');
const EventRepository = require('@repos/event-repository');
const moment = require('moment');

Model.knex(db);

async function getUpcomingEvents() {
    const now = moment()
        .local()
        // .add(config.scheduler.lookForward, 'm')
        .format('YYYY-MM-DD HH:mm:ss');
    return await EventRepository.findAllPotentialUpcomingByDate(now);
}

module.exports = {
    getUpcomingEvents
};


