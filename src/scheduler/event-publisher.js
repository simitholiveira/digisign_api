const moment = require('moment');
const EventPublisher = require('@utils/event-publisher');

const eventPublisher = new EventPublisher;

eventPublisher.on('beforeScanEvents', (props) => {
    console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
    const now = moment().local().format('YYYY-MM-DD HH:mm:ss');
    console.log('NOW: ', now);
    console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
});

eventPublisher.on('beforeScheduleEvent', (props) => {
    console.log('------------------------------------------');
    console.log('BEFORE SCHEDULE EVENT: ', props.event.getId());
});

eventPublisher.on('beforeRunEvent', (props) => {
    console.log('------------------------------------------');
    console.log('BEFORE RUN EVENT: ', props.event.getId());
});

eventPublisher.on('beforeSuspendEvent', (props) => {
    console.log('------------------------------------------');
    console.log('BEFORE SUSPEND EVENT: ', props.event.getId());
});

eventPublisher.on('beforeTerminateEvent', (props) => {
    console.log('------------------------------------------');
    console.log('BEFORE TERMINATE EVENT: ', props.event.getId());
});

eventPublisher.on('afterTerminateEvent', (props) => {
    console.log('------------------------------------------');
    console.log('AFTER TERMINATE EVENT: ', props.event.getId());
});

eventPublisher.on('beforeCancelEvent', (props) => {
    console.log('------------------------------------------');
    console.log('BEFORE CANCEL EVENT: ', props.event.getId());
});

eventPublisher.on('afterCreateEventRecurrenceRules', (props) => {
    console.log('------------------------------------------');
    console.log('AFTER CREATE REC RULE: ', props.event.getId());
    console.log("Recurrence rule: ", [...props.rules]);
});

eventPublisher.on('beforeSettingEventSuspendingTimer', (props) => {
    console.log('------------------------------------------');
    console.log('BEFORE SET SUSPENDING TIMER: ', props.event.getId());
    console.log("Event duration: " + props.duration + ' ms.');
});

eventPublisher.on('beforeShowSlide', (props) => {
    console.log('------------------------------------------');
    const slide = props.playlistSlide.getSlide();
    console.log('BEFORE RUN SLIDE: ', slide.getId());
    console.log("Slide duration: " + slide.getDurationInMilliseconds() + ' ms.');
    console.log("Is suspended: " + props.isSuspended);
});

eventPublisher.on('error', (error) => {
    console.error(error.message);
});

module.exports = eventPublisher;
