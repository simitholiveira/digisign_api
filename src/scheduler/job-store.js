class JobStore {
    constructor() {
        this.store = new Map();
    }

    set(id, data) {
        let value = this.store.get(id) || {};
        value = Object.assign(value, data);
        this.store.set(id, value);
    }

    get(id) {
        return this.store.get(id);
    }

    has(id) {
        return this.store.has(id);
    }

    delete(id) {
        return this.store.delete(id);
    }

    forEach(fn) {
        return this.store.forEach(fn);
    }

    keys() {
        return this.store.keys();
    }
}

module.exports = JobStore;
