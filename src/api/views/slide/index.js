
function SlideView(slide) {
    return {
        id: slide.getExternalId(),
        name: slide.getName(),
        thumbnail: slide.getThumbnail(),
        durationInSeconds: slide.getDuration(),
        highest_zIndex: slide.getHighestZIndex(),
        assets: slide.getAssets(),
        css: slide.getCss(),
        styles: slide.getStyles(),
        components: slide.getComponents(),
        files: slide.files,
    };
}

module.exports = {
    SlideView,
};
