const {
    sanitizeOutputParentId,
    sanitizeOutputEventLength,
    sanitizeStringToJson
} = require('@utils/event');

function EventView(event) {
    return {
        id: event.getId(),
        start_date: event.getStartAt(),
        end_date: event.getEndAt(),
        text: event.getText(),
        event_pid: sanitizeOutputParentId(event.getEventParentId()),
        event_length: sanitizeOutputEventLength(event.getEventLength()),
        rec_pattern: event.getRecurrencePattern(),
        rec_type: event.getRecurrenceType(),
        time: sanitizeStringToJson(event.getTime()),
        play_content: sanitizeStringToJson(event.getPlayContent()),
        content_type: sanitizeStringToJson(event.getContentType()),
    };
}

module.exports = {
    EventView,
};
