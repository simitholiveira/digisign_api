
function PlaylistView(playlist) {
    return {
        id: playlist.external_id,
        name: playlist.name,
        slides: playlist.playlistSlides.map(playlistSlide => PlaylistSlideView(playlistSlide)),
    };
}

function PlaylistSlideView(playlistSlide) {
    return {
        id: playlistSlide.slide.external_id,
    }
}

module.exports = {
    PlaylistView,
    PlaylistSlideView,
};
