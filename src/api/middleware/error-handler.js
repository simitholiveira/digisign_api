/**
 * Custom error handler
 * Uses custom method `error` to send response
 */

module.exports = function(err, req, res, next) {
    console.error(err);
    if (res.headersSent) {
        return next(err);
    }
    return res.error(err);
};
