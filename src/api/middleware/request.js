/**
 * Add helper methods to request
 */

module.exports = function(req, res, next) {
    req.getAppConfig = function() {
        return req.app.locals.config;
    };
    next();
};
