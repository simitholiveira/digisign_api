/**
 * Authenticate user by 1) username, password 2) access token
 * Current user is injected into request object
 */

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;

const UserRepository = require('../../repositories/user-repository');

const users = new UserRepository();

passport.use('local', new LocalStrategy(
    {
        usernameField: 'username',
        passwordField: 'password',
    },
    function(username, password, done) {
        users.findByUsername(username)
            .then(user => {
                if (!user || !user.validatePassword(password)) {
                    return done(null, false/*, { message: 'Invalid credentials' }*/);
                }
                return done(null, user);
            })
            .catch(err => done(err, false));
    }
));

passport.use('token', new BearerStrategy(function(token, done) {
    token = token.toString();
    if (!token.length) {
        return done(null, false);
    }
    users.findByAccessToken(token)
        .then(user => {
            if (!user) {
                return done(null, false);
            }
            return done(null, user);
        })
        .catch(err => done(err, false));
}));

/**
 * Use custom auth handler because default handler does not know we want to respond with json
 */
function authenticate(type, options) {
    return function(req, res, next) {
        passport.authenticate(type, options, handler)(req, res, next);

        function handler(err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.notAuthorized();
            }
            req.user = user;
            next();
        }
    };
}

module.exports = {
    localAuth: authenticate('local', {session: false}),
    tokenAuth: authenticate('token', {session: false}),
};
