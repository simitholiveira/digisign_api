/**
 * Add helper methods to response
 */

module.exports = function(req, res, next) {
    res.ok = function(data, code = 200) {
        ok(data, code);
    };
    res.error = function(data, code = 500) {
        error(data, code);
    };
    res.badRequest = function(data = 'Bad Request') {
        error(data, 400);
    };
    res.notAuthorized = function(data = 'Not Authorized') {
        error(data, 401);
    };
    res.notFound = function(data = 'Not Found') {
        error(data, 404);
    };
    res.unprocessableEntity = function(data = 'Unprocessable Entity') {
        error(data, 422);
    };

    next();

    function ok(data, code = 200) {
        res
            .status(code)
            .json(data);
    }

    function error(error, code = 500) {
        res
            .status(code)
            .json({error});
    }
};
