const router = require('express').Router();
const swaggerUi = require('swagger-ui-express');

router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', function (req, res) {
    const fs = require('fs');
    const yaml = require('js-yaml');
    let config = fs.readFileSync(__dirname + '/../views/doc/config.yaml', 'utf8');
    config = yaml.safeLoad(config);
    return swaggerUi.setup(config)(req, res);
});

module.exports = router;
