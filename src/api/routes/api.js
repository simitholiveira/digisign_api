const Router = require("express-async-router").AsyncRouter;
const router = Router();
const bodyParser = require('body-parser');
const os = require('os');
const multer = require('multer');
const upload = multer({dest: os.tmpdir()});
const cors = require('cors');
const passport = require('passport');

const SlideController = require('@api/controllers/slide-controller');
const EventController = require('@api/controllers/event-controller');
const EventsListController = require('@api/controllers/events-list-controller');
const AuthController = require('@api/controllers/auth-controller');
const DeviceController = require('@api/controllers/device-controller');
const PlaylistController = require('@api/controllers/playlist-controller');
const SlideValidationRules = require('@api/validations/slide-validation-rules');
const PlaylistValidationRules = require('@api/validations/playlist-validation-rules');
const EventValidationRules = require('@api/validations/event-validation-rules');
const EventsListValidationRules = require('@api/validations/events-list-validation-rules');

const {localAuth, tokenAuth} = require('../middleware/auth');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));
router.use(cors());
router.use(passport.initialize());


const auth = Router();
router.use('/auth', auth);

auth.post('/pair', localAuth, AuthController.pair);
auth.post('/change-password', tokenAuth, AuthController.changePassword);
auth.post('/unpair', tokenAuth, AuthController.unpair);

const slides = Router();
router.use('/slide', tokenAuth, slides);

const slideFiles = upload.any([{name: 'html', maxCount: 1}, {name: 'media'}]);
const uploadSlideFile = upload.any([{name: 'file', maxCount: 1}]);

slides.get('/', SlideController.list);
slides.post('/', [
    slideFiles,
    SlideValidationRules.getRulesForMethodCreate()
], SlideController.create);
slides.patch('/:id', [
    slideFiles,
    SlideValidationRules.getRulesForMethodUpdate()
], SlideController.update);
slides.get('/:id', [
    SlideValidationRules.getRulesForMethodGetByName()
], SlideController.view);
slides.post('/:id/upload-file', [
    uploadSlideFile,
    SlideValidationRules.getRulesForMethodUploadFile()
], SlideController.uploadFile);
slides.post('/:id/play', [
    upload.none(),
    SlideValidationRules.getRulesForMethodPlayByName()
], SlideController.showNow);

const playlist = Router();
router.use('/playlist', tokenAuth, playlist);

playlist.get('/', PlaylistController.list);
playlist.post('/', [
    upload.none(),
    PlaylistValidationRules.getRulesForMethodCreate()
], PlaylistController.create);
playlist.put('/', [
    upload.none(),
    PlaylistValidationRules.getRulesForMethodPut()
], PlaylistController.put);
playlist.get('/:id', [
    PlaylistValidationRules.getRulesForMethodGetByName()
], PlaylistController.view);
playlist.post('/:id/play', [
    upload.none(),
    PlaylistValidationRules.getRulesForMethodPlayByName()
], PlaylistController.playNow);

const event = Router();
router.use('/event', tokenAuth, event);

event.post('/', [
    upload.none(),
    EventValidationRules.getRulesForMethodCreate()
], EventController.create);
event.get('/:id', [
    EventValidationRules.getRulesForMethodGet()
], EventController.view);
event.patch('/:id', [
    EventValidationRules.getRulesForMethodUpdate()
], EventController.update);
event.delete('/:id', [
    EventValidationRules.getRulesForMethodDelete()
], EventController.remove);

const eventsList = Router();
router.use('/events-list', tokenAuth, eventsList);

eventsList.get('/', EventsListController.view);
eventsList.post('/', [
    upload.none(),
    EventsListValidationRules.getRulesForMethodCreate()
], EventsListController.create);

const device = Router();
router.use('/device', tokenAuth, device);

device.post('/blink', DeviceController.blink);
device.get('/info', DeviceController.info);
device.get('/info/net', DeviceController.net);

module.exports = router;
