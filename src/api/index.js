const express = require('express');

const config = require('@config');
require('@src/bootstrap').db(config.db);

const apiRouter = require('./routes/api');
const swaggerRouter = require('./routes/swagger');

const app = express();

app.locals.config = config;

app.use(express.static(__dirname + '/../../storage'));
app.use(require('./middleware/request'));
app.use(require('./middleware/response'));

app.use('/', swaggerRouter);
app.use('/api', apiRouter);

/**
 * Error handler
 */
app.use(require('./middleware/error-handler'));

module.exports = app;
