const { createEvents } = require('@usecases/event/create-events-list');
const { verifyRequestDataCorrect } = require('@api/validations/request-validator');
const EventRepository = require('@repos/event-repository');
const { EventView } = require('@api/views/event');

async function view(req, res) {
    const lists = await EventRepository.findAll();
    res.ok(lists.map(list => EventView(list)));
}

async function create(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await createEvents(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}


module.exports = {
    view,
    create,
};
