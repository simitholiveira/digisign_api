const SlideRepository = require('@repos/slide-repository');
const { verifyRequestDataCorrect } = require('@api/validations/request-validator');
const { createSlide } = require('@usecases/slide/create-slide');
const { updateSlide } = require('@usecases/slide/update-slide');
const { uploadSlideFile } = require('@usecases/slide/upload-slide-file');
const { showSlide } = require('@usecases/slide/show-slide');
const { SlideView } = require('@api/views/slide');


async function list(req, res) {
    const slides = await SlideRepository.findAll();
    res.ok(slides.map(slide => SlideView(slide)));
}

async function view(req, res) {
    try {
        verifyRequestDataCorrect(req);
        const slide = await SlideRepository.findByExternalId(req.params.id);
        res.ok(SlideView(slide));
    } catch (err) {
        res.notFound(err.message);
    }
}

async function create(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await createSlide(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function update(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await updateSlide(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function uploadFile(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await uploadSlideFile(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function showNow(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await showSlide(req);
        res.ok();
    } catch (err) {
        res.badRequest(err.message);
    }
}


module.exports = {
    list,
    view,
    create,
    update,
    uploadFile,
    showNow,
};
