const UserRepository = require('../../repositories/user-repository');
const {isEmpty} = require('../../util');
const UserService = require('../../services/user-service');

const userRepository = new UserRepository();
const userService = new UserService(userRepository);

async function pair(req, res) {
    const user = req.user;
    const config = req.getAppConfig();
    await userService.generateAccessToken(user, config.user.regenerateToken);
    res.ok(user.access_token);
}

async function changePassword(req, res) {
    const user = req.user;
    await userService.changePassword(user, req.body.new_password);
    res.ok();
}

async function unpair(req, res) {
    const user = req.user;
    await userService.resetAccessToken(user);
    res.ok();
}

module.exports = {
    pair,
    changePassword,
    unpair,
};
