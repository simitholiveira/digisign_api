const os = require('os');
const sysInfo = require('systeminformation');
const Browser = require('@services/chrome-service');
const {isTrue} = require('@utils');
const {getUniqueDeviceId} = require('@src/services/device-service');
const version = require('@root/version');

async function blink(req, res) {
    /**
     * TODO will interrupt current slide
     * TODO when turned off will go to default page
     */
    const config = req.getAppConfig();

    if (isTrue(req.body.blink)) {
        await blinkOn();
    } else {
        await blinkOff();
    }
    res.ok();

    async function blinkOn() {
        let blinkUrl = config.chrome.blinkUrl;
        if (req.body.message) {
            blinkUrl += `#${req.body.message}`;
        }
        blinkUrl = encodeURI(blinkUrl);
        await Browser.showScreen(blinkUrl);
    }

    async function blinkOff() {
        await Browser.showScreen(config.chrome.defaultUrl);
    }
}

async function info(req, res) {
    return res.ok({
        deviceId: await getUniqueDeviceId(),
        version,
        hostname: os.hostname(),
        date: (new Date),
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        os: await sysInfo.osInfo(),
        displays: (await sysInfo.graphics()).displays,
        disks: await sysInfo.diskLayout(),
        fs: await sysInfo.fsSize(),
        memory: await sysInfo.mem(),
    });
}

async function net(req, res) {
    return res.ok({
        net: await sysInfo.networkInterfaces(),
        wifi: await sysInfo.wifiNetworks(),
    });
}

module.exports = {
    blink,
    info,
    net,
};
