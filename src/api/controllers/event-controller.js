const { createEvent } = require('@usecases/event/create-event');
const { updateEvent } = require('@usecases/event/update-event');
const { removeEvent } = require('@usecases/event/remove-event');
const { verifyRequestDataCorrect } = require('@api/validations/request-validator');
const EventRepository = require('@repos/event-repository');
const { EventView } = require('@api/views/event');


async function view(req, res) {
    try {
        verifyRequestDataCorrect(req);
        const event = await EventRepository.findById(req.params.id);
        res.ok(EventView(event));
    } catch (err) {
        res.notFound(err.message);
    }
}

async function create(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await createEvent(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function update(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await updateEvent(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function remove(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await removeEvent(req);
        res.ok({success: true});
    } catch (err) {
        res.notFound(err.message);
    }
}

module.exports = {
    view,
    create,
    update,
    remove,
};
