const PlaylistRepository = require('@repos/playlist-repository');
const { PlaylistView } = require('@api/views/playlist');
const { verifyRequestDataCorrect } = require('@api/validations/request-validator');
const { createPlaylist } = require('@usecases/playlist/create-playlist');
const { updatePlaylist } = require('@usecases/playlist/update-playlist');
const { playPlaylist } = require('@usecases/playlist/play-playlist');
const { NotFoundError } = require('@errors');


async function list(req, res) {
    const lists = await PlaylistRepository.findAll();
    res.ok(lists.map(list => PlaylistView(list)));
}

async function view(req, res) {
    try {
        verifyRequestDataCorrect(req);
        const playlist = await PlaylistRepository.findByExternalId(req.params.id);
        res.ok(PlaylistView(playlist));
    } catch (err) {
        res.notFound(err.message);
    }
}

async function create(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await createPlaylist(req);
        res.ok({success: true});
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function put(req, res) {
    try {
        verifyRequestDataCorrect(req);
        try {
            await updatePlaylist(req);
            res.ok({success: true});
        } catch (err) {
            if (err instanceof NotFoundError) {
                await createPlaylist(req);
                res.ok({success: true}, 201);
            } else {
                throw err;
            }
        }
    } catch (err) {
        res.badRequest(err.message);
    }
}

async function playNow(req, res) {
    try {
        verifyRequestDataCorrect(req);
        await playPlaylist(req);
        res.ok();
    } catch (err) {
        res.badRequest(err.message);
    }
}

module.exports = {
    create,
    put,
    view,
    playNow,
    list,
};
