const { body, param } = require('express-validator');
const EventRepository = require('@repos/event-repository');
const {
    isChildEvent,
    sanitizeInputParentId,
    isInputEventLengthValid,
    sanitizeInputEventLength,
    sanitizeJsonToString,
    assertEventContentTypeIsValid
} = require('@utils/event');
const moment = require('moment');
const RecTypeParser = require('@services/event/rec-type-parsers/rec-type-parser');


function getRulesForMethodGet() {
    return [
        param('id', 'Invalid id')
            .exists()
            .isInt(),
    ];
}

function getRulesForMethodCreate() {
    return [

        body('id', 'Invalid id')
            .exists()
            .isInt()
            .custom(async (val) => {
                await EventRepository.assertEventIdNotExist(val);
            }),

        body('start_date', 'Invalid start_date')
            .exists()
            .customSanitizer(val => moment(val).toDate()),

        body('end_date', 'Invalid end_date')
            .exists()
            .customSanitizer(val => moment(val).toDate()),

        body('text', 'Invalid text').exists(),

        body('content_type', 'Invalid content_type')
            .exists()
            .custom(async (val) => {
                await assertEventContentTypeIsValid(val);
                return true;
            }),

        body('event_length', 'Invalid event_length')
            .exists()
            .custom(val => isInputEventLengthValid(val))
            .customSanitizer(val => sanitizeInputEventLength(val)),

        body('event_pid', 'Invalid event_pid')
            .exists()
            .custom((val) => {
                return !isChildEvent(val);
            })
            .customSanitizer(val => sanitizeInputParentId(val)),

        body('rec_type', 'Invalid rec_type')
            .exists()
            .custom(val => {
                if ('' !== val) {
                    new RecTypeParser(val);
                }
                return true;
            }),

        body('rec_pattern', 'Invalid rec_pattern')
            .exists()
            .custom(val => {
                if ('' !== val) {
                    new RecTypeParser(val);
                }
                return true;
            }),

        body('time', 'Invalid time')
            .exists()
            .isLength({max: 255})
            .customSanitizer(val => sanitizeJsonToString(val)),

        body('play_content', 'Invalid play_content')
            .exists()
            .isLength({max: 1000})
            .customSanitizer(val => sanitizeJsonToString(val)),
    ];
}

function getRulesForMethodUpdate() {
    return [

        param('id', 'Invalid id')
            .exists()
            .isInt()
            .custom(async (val) => {
                await EventRepository.findById(val);
                return true;
            }),

        body('start_date', 'Invalid start_date')
            .optional()
            .customSanitizer(val => moment(val).toDate()),

        body('end_date', 'Invalid end_date')
            .optional()
            .customSanitizer(val => moment(val).toDate()),

        body('text', 'Invalid text').optional(),

        body('content_type', 'Invalid content_type')
            .optional()
            .custom(async (val) => {
                await assertEventContentTypeIsValid(val);
                return true;
            }),

        body('event_length', 'Invalid event_length')
            .optional()
            .custom(val => isInputEventLengthValid(val))
            .customSanitizer(val => sanitizeInputEventLength(val)),

        body('event_pid', 'Invalid event_pid')
            .optional()
            .custom((val) => {
                return !isChildEvent(val);
            })
            .customSanitizer(val => sanitizeInputParentId(val)),

        body('rec_type', 'Invalid rec_type')
            .optional()
            .custom(val => {
                if ('' !== val) {
                    new RecTypeParser(val);
                }
                return true;
            }),

        body('rec_pattern', 'Invalid rec_pattern')
            .optional()
            .custom(val => {
                if ('' !== val) {
                    new RecTypeParser(val);
                }
                return true;
            }),

        body('time', 'Invalid time')
            .optional()
            .isLength({max: 255})
            .customSanitizer(val => sanitizeJsonToString(val)),

        body('play_content', 'Invalid play_content')
            .optional()
            .isLength({max: 1000})
            .customSanitizer(val => sanitizeJsonToString(val)),
    ];
}

function getRulesForMethodDelete() {
    return [

        param('id', 'Invalid id')
            .exists()
            .isInt()
            .custom(async (val) => {
                await EventRepository.findById(val);
                return true;
            }),

    ];
}

module.exports = {
    getRulesForMethodGet,
    getRulesForMethodCreate,
    getRulesForMethodUpdate,
    getRulesForMethodDelete,
};
