const { ScheduleCollisionError } = require('@errors');


class EventScheduleCollisionValidator {
    constructor(event) {
        this._event = event;
    }

    setScheduleCollisionChecker(scheduleCollisionChecker) {
        this._scheduleCollisionChecker = scheduleCollisionChecker;
    }

    setPotentialOverlappedEvents(events) {
        this._potentialOverlappedEvents = events;
    }

    setEventScheduleBuilder(eventScheduleBuilder) {
        this._eventScheduleBuilder = eventScheduleBuilder;
    }

    assertCollisionIsAbsent() {

        this._initCollisionChecker();

        for (let event of this._potentialOverlappedEvents) {

            if (event.getId() === this._event.getId()) {
                continue;
            }

            this._assertEventDoesNotHaveScheduleCollision(event);
        }
    }

    _initCollisionChecker() {
        const schedule = this._getEventSchedule(this._event);

        this._scheduleCollisionChecker.setFirstSchedule(schedule);
    }

    _assertEventDoesNotHaveScheduleCollision(event) {
        const schedule = this._getEventSchedule(event);

        this._scheduleCollisionChecker.setSecondSchedule(schedule);

        if (this._scheduleCollisionChecker.hasCollision()) {
            throw new ScheduleCollisionError;
        }
    }

    _getEventSchedule(event) {
        this._eventScheduleBuilder.setEvent(event);
        return this._eventScheduleBuilder.getSchedule();
    }
}

module.exports = EventScheduleCollisionValidator;
