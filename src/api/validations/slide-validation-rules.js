const { body, param } = require('express-validator');
const File = require('@utils/file');
const { filterFilesByFieldName } = require('@utils/request');
const { InvalidDataError } = require('@errors');
const { isDefined, isEmpty } = require('@utils');
const SlideRepository = require('@repos/slide-repository');
const config = require('@config');


function isFileHTML(file) {
    return File.isHTMLByFileName(file.originalname)
        && File.isHTMLByMimeType(file.mimetype);
}

function isFileImage(file) {
    return File.isImageByFileName(file.originalname)
        && File.isImageByMimeType(file.mimetype);
}

function isFileVideo(file) {
    return File.isVideoByFileName(file.originalname)
        && File.isVideoByMimeType(file.mimetype);
}

function assertFileIsHtml(file) {
    if (!isDefined(file) || !isFileHTML(file)) {
        throw new InvalidDataError('Only HTML format allowed.');
    }
}

function assertFileIsMedia(file) {
    if (!isFileImage(file) && !isFileVideo(file)) {
        throw new InvalidDataError('Only HTML format allowed.');
    }
}

function assertArrayNotGreater(arr, expectedSize) {
    if (arr.length > expectedSize) {
        throw new InvalidDataError(`Only ${expectedSize} elements allowed.`);
    }
}


function getRulesForMethodGetByName() {
    return [
        param('id', 'Invalid ID')
            .exists(),
    ];
}

function getRulesForMethodCreate() {
    return [

        body('id', 'Invalid ID')
            .exists()
            .notEmpty()
            .isLength({max: 50})
            .custom(async (val) => {
                await SlideRepository.assertSlideExternalIdNotExist(val);
            }),

        body('durationInSeconds', 'Invalid duration')
            .exists()
            .isInt({min: config.slide.minDurationInSeconds}),

        body('html', 'Invalid html')
            .custom((val, {req}) => {
                if (!isDefined(req.files)) {
                    throw new InvalidDataError('HTML file is required.');
                }
                const files = filterFilesByFieldName(req.files, 'html');
                assertArrayNotGreater(files, 1);
                const [html] = files;
                assertFileIsHtml(html);
                return true;
            }),

        body('media', 'Invalid media')
            .custom((val, {req}) => {
                if (!isDefined(req.files)) {
                    return true;
                }
                const files = filterFilesByFieldName(req.files, 'media');
                assertArrayNotGreater(files, config.slide.limitMediaFilesInOneRequest);
                for (let file of files) {
                    assertFileIsMedia(file);
                }
                return true;
            }),

        body('highest_zIndex', 'Invalid highest_zIndex')
            .optional()
            .isInt(),

        body('name', 'Invalid name')
            .optional()
            .isLength({max: 100}),

        body('thumbnail', 'Invalid thumbnail')
            .optional()
            .isLength({max: 100}),
    ];
}

function getRulesForMethodUpdate() {
    return [

        param('id', 'Invalid ID')
            .exists()
            .notEmpty()
            .isLength({max: 50})
            .custom(async (val, {req}) => {
                await SlideRepository.findByExternalId(val);
                return true;
            }),

        body().custom((val, {req}) => {
            return !isEmpty(req.body) || !isEmpty(req.files);
        }),

        body('durationInSeconds', 'Invalid duration')
            .optional()
            .isInt({min: config.slide.minDurationInSeconds}),

        body('html', 'Invalid html')
            .custom((val, {req}) => {
                if (!isDefined(req.files)) {
                    return true;
                }
                const files = filterFilesByFieldName(req.files, 'html');
                if (0 === files.length) {
                    return true;
                }
                assertArrayNotGreater(files, 1);
                const [html] = files;
                assertFileIsHtml(html);
                return true;
            }),

        body('media', 'Invalid media')
            .custom((val, {req}) => {
                if (!isDefined(req.files)) {
                    return true;
                }
                const files = filterFilesByFieldName(req.files, 'media');
                assertArrayNotGreater(files, config.slide.limitMediaFilesInOneRequest);
                for (let file of files) {
                    assertFileIsMedia(file);
                }
                return true;
            }),

        body('highest_zIndex', 'Invalid highest_zIndex')
            .optional()
            .isInt(),

        body('name', 'Invalid name')
            .optional()
            .isLength({max: 100}),

        body('thumbnail', 'Invalid thumbnail')
            .optional()
            .isLength({max: 100}),
    ];
}

function getRulesForMethodUploadFile() {
    return [

        param('id', 'Invalid ID')
            .exists()
            .isLength({max: 50}),

        body('media', 'Invalid media')
            .custom((val, {req}) => {
                if (!isDefined(req.files)) {
                    throw new InvalidDataError('Media file is required.');
                }
                const [file] = filterFilesByFieldName(req.files, 'file');
                if (!isDefined(file)) {
                    throw new InvalidDataError('Media file is required.');
                }
                if (!isFileImage(file) && !isFileVideo(file)) {
                    throw new InvalidDataError('Only media format allowed.');
                }
                return true;
            }),
    ];
}

function getRulesForMethodPlayByName() {
    return [
        param('id', 'Invalid ID')
            .exists(),
    ];
}

module.exports = {
    getRulesForMethodGetByName,
    getRulesForMethodCreate,
    getRulesForMethodUpdate,
    getRulesForMethodUploadFile,
    getRulesForMethodPlayByName,
};
