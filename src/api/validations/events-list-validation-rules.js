const { body } = require('express-validator');
const PlaylistRepository = require('@repos/playlist-repository');
const {
    isChildEvent,
    sanitizeInputParentId,
    isInputEventLengthValid,
    sanitizeInputEventLength,
    sanitizeJsonToString,
    assertEventContentTypeIsValid
} = require('@utils/event');
const moment = require('moment');
const RecTypeParser = require('@services/event/rec-type-parsers/rec-type-parser');


function getRulesForMethodCreate() {
    return [

        body('events.*.id', 'Invalid id')
            .exists()
            .isInt(),

        body('events.*.start_date', 'Invalid start_date')
            .exists()
            .customSanitizer(val => moment(val).toDate()),

        body('events.*.end_date', 'Invalid end_date')
            .exists()
            .customSanitizer(val => moment(val).toDate()),

        body('events.*.text', 'Invalid text').exists(),

        body('events.*.content_type', 'Invalid content_type')
            .exists()
            .custom(async (val) => {
                await assertEventContentTypeIsValid(val);
                return true;
            }),

        body('events.*.event_length', 'Invalid event_length')
            .exists()
            .custom(val => isInputEventLengthValid(val))
            .customSanitizer(val => sanitizeInputEventLength(val)),

        body('events.*.event_pid', 'Invalid event_pid')
            .exists()
            .custom((val) => {
                return !isChildEvent(val);
            })
            .customSanitizer(val => sanitizeInputParentId(val)),

        body('events.*.rec_type', 'Invalid rec_type')
            .exists()
            .custom(val => {
                if ('' !== val) {
                    new RecTypeParser(val);
                }
                return true;
            }),

        body('events.*.rec_pattern', 'Invalid rec_pattern')
            .exists()
            .custom(val => {
                if ('' !== val) {
                    new RecTypeParser(val);
                }
                return true;
            }),

        body('events.*.time', 'Invalid time')
            .exists()
            .isLength({max: 255})
            .customSanitizer(val => sanitizeJsonToString(val)),

        body('events.*.play_content', 'Invalid play_content')
            .exists()
            .isLength({max: 1000})
            .customSanitizer(val => sanitizeJsonToString(val)),
    ];
}


module.exports = {
    getRulesForMethodCreate,
};
