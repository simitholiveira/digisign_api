const { body, param } = require('express-validator');
const { isUndefined } = require('@utils');
const { InvalidDataError } = require('@errors');
const SlideRepository = require('@repos/slide-repository');
const PlaylistRepository = require('@repos/playlist-repository');


function getRulesForMethodGetByName() {
    return [
        param('id', 'Invalid ID')
            .exists()
            .notEmpty(),
    ];
}

function getRulesForMethodCreate() {
    return [

        body('id', 'Invalid ID')
            .exists()
            .notEmpty()
            .isLength({max: 50})
            .custom(async (val) => {
                await PlaylistRepository.assertPlaylistExternalIdNotExist(val);
            }),

        body('slides', 'Invalid slides')
            .exists()
            .notEmpty(),

        body('slides.*.id', 'Invalid slides')
            .exists()
            .notEmpty()
            .custom(async (val) => {
                await SlideRepository.findByExternalId(val);
                return true;
            }),
    ];
}

function getRulesForMethodPut() {
    return [

        body('id', 'Invalid ID')
            .exists()
            .notEmpty()
            .isLength({max: 50}),

        body('slides', 'Invalid slides')
            .exists()
            .notEmpty(),

        body('slides.*.id', 'Invalid slides')
            .exists()
            .notEmpty()
            .custom(async (val) => {
                await SlideRepository.findByExternalId(val);
                return true;
            }),
    ];
}

function getRulesForMethodPlayByName() {
    return [
        param('id', 'Invalid ID')
            .exists()
            .notEmpty(),
    ];
}

module.exports = {
    getRulesForMethodGetByName,
    getRulesForMethodCreate,
    getRulesForMethodPut,
    getRulesForMethodPlayByName,
};
