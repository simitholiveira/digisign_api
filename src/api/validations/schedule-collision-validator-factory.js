const EventScheduleBuilderWrapper = require('@services/event/schedule-builders/event-schedule-builder-wrapper');
const ScheduleCollisionChecker = require('@services/event/schedule-collision-checker');
const ScheduleBuilderFactory = require('@services/event/schedule-builders/schedule-builder-factory');
const EventScheduleCollisionValidator = require('@api/validations/schedule-collision-validator');


class EventScheduleCollisionValidatorFactory {
    constructor(event) {
        this._event = event;
    }

    setPotentialOverlappedEventsProvider(provider) {
        this._providePotentialOverlappedEvents = provider;
    }

    async createValidator() {

        const collisionValidator = new EventScheduleCollisionValidator(this._event);

        collisionValidator.setScheduleCollisionChecker(new ScheduleCollisionChecker);

        const events = await this._getPotentialOverlappedEvents();
        collisionValidator.setPotentialOverlappedEvents(events);

        const eventScheduleBuilder = this._getEventScheduleBuilder();
        collisionValidator.setEventScheduleBuilder(eventScheduleBuilder);

        return collisionValidator;
    }

    async _getPotentialOverlappedEvents() {
        return await this._providePotentialOverlappedEvents(this._event);
    }

    _getEventScheduleBuilder() {
        const eventScheduleBuilder = new EventScheduleBuilderWrapper;
        eventScheduleBuilder.setScheduleBuilderFactory(new ScheduleBuilderFactory);
        return eventScheduleBuilder;
    }
}

module.exports = EventScheduleCollisionValidatorFactory;
