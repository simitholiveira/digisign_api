const { validationResult } = require('express-validator');
const { InvalidDataError } = require('@errors');


function verifyRequestDataCorrect(req) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const [error] = errors.array();
        throw new InvalidDataError(error.msg, error);
    }
}

module.exports = {
    verifyRequestDataCorrect,
};
