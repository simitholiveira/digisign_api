const EventScheduleCollisionValidatorFactory = require('@api/validations/schedule-collision-validator-factory');
const EventRepository = require('@repos/event-repository');


async function assertEventScheduleDoesntOverlapStored(event) {
    const collisionValidatorFactory = new EventScheduleCollisionValidatorFactory(event);
    collisionValidatorFactory.setPotentialOverlappedEventsProvider(async event => {
        return await EventRepository.findAllOverlappedByEvent(event);
    });
    const collisionValidator = await collisionValidatorFactory.createValidator();
    collisionValidator.assertCollisionIsAbsent();
}

async function assertEventsListSchedulesNotOverlapped(events) {
    for (let event of events) {
        const collisionValidatorFactory = new EventScheduleCollisionValidatorFactory(event);
        collisionValidatorFactory.setPotentialOverlappedEventsProvider(() => {
            return events;
        });
        const collisionValidator = await collisionValidatorFactory.createValidator();
        collisionValidator.assertCollisionIsAbsent();
    }
}

module.exports = {
    assertEventScheduleDoesntOverlapStored,
    assertEventsListSchedulesNotOverlapped,
};
