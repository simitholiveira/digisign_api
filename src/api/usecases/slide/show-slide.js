const SlideRepository = require('@repos/slide-repository');
const Browser = require('@services/chrome-service');
const EventFacadeBuilder = require('@services/event/facade/event-facade-builder');


async function showSlide(request) {
    const slide = await SlideRepository.findByExternalId(request.params.id);
    Browser.showSlide(EventFacadeBuilder.createSlideFacade(slide));
}

module.exports = {
    showSlide
};
