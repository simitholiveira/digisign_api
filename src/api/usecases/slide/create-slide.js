const SlideRepository = require('@repos/slide-repository');
const { SlideService, createFileFromUpload } = require('@services/slide-service');
const { filterFilesByFieldName } = require('@utils/request');

const slideService = new SlideService(new SlideRepository);


async function createSlide(request) {

    let [htmlFile] = filterFilesByFieldName(request.files, 'html');
    htmlFile = createFileFromUpload(htmlFile);

    let mediaFiles = filterFilesByFieldName(request.files, 'media');
    mediaFiles = mediaFiles.map(upload => createFileFromUpload(upload));

    await slideService.createSlide(request.body, htmlFile, mediaFiles);
}

module.exports = {
    createSlide,
};
