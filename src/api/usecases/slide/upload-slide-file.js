const SlideRepository = require('@repos/slide-repository');
const { SlideService, createFileFromUpload } = require('@services/slide-service');

const slideService = new SlideService(new SlideRepository);


async function uploadSlideFile(request) {

    const slide = await SlideRepository.findByExternalId(request.params.id);
    const files = request.files.map(upload => createFileFromUpload(upload));

    await slideService.updateFiles(slide, files);
}

module.exports = {
    uploadSlideFile,
};
