const SlideRepository = require('@repos/slide-repository');
const { createFileFromUpload } = require('@services/slide-service');
const { isDefined } = require('@utils');
const { filterFilesByFieldName } = require('@utils/request');
const { setRequestDataToSlide } = require('@services/slide/slide-config-service');


const slideRepository = new SlideRepository;

async function updateSlide(request) {
    const slide = await SlideRepository.findByExternalId(request.params.id);
    setRequestDataToSlide(request.body, slide);

    if (isDefined(request.files)) {
        let [htmlFile] = filterFilesByFieldName(request.files, 'html');
        if (isDefined(htmlFile)) {
            slide.addFile(createFileFromUpload(htmlFile));
        }
        let mediaFiles = filterFilesByFieldName(request.files, 'media');
        if (isDefined(mediaFiles)) {
            mediaFiles.map(upload => {
                slide.addFile(createFileFromUpload(upload));
            });
        }
    }

    await slideRepository.update(slide);
}

module.exports = {
    updateSlide,
};
