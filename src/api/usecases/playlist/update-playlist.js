const PlaylistRepository = require('@repos/playlist-repository');
const PlaylistService = require('@services/playlist-service');
const { createPlaylistSlideByExternalSlideId } = require('@services/playlist-slide-service');

const playlistService = new PlaylistService(new PlaylistRepository);

async function updatePlaylist(request) {
    const playlist = await PlaylistRepository.findByExternalId(request.body.id);
    const playlistSlides = [];
    for (const slide of request.body.slides) {
        playlistSlides.push(await createPlaylistSlideByExternalSlideId(slide.id));
    }
    await playlistService.updatePlaylist(playlist, playlistSlides);
}


module.exports = {
    updatePlaylist,
};
