const PlaylistRepository = require('@repos/playlist-repository');
const { closeWindow } = require('@services/chrome-service');
const { PlaylistPlayer } = require('@services/playlist-player');
const Iterator = require('@utils/iterator');
const { inArray } = require('@utils');
const EventFacadeBuilder = require('@services/event/facade/event-facade-builder');


async function playPlaylist(request) {
    const pl = await PlaylistRepository.findByExternalId(request.params.id);
    const plFacades = pl.playlistSlides.map(item => {
        return EventFacadeBuilder.createPlaylistSlideFacade(item)
    });
    const iterator = new Iterator(plFacades);
    const player = new PlaylistPlayer(iterator);

    const shownPlaylistSlideIds = [];
    player.setIsPlayingMustBeSuspendedFunction((playlistSlide) => {
        const isSuspended = inArray(playlistSlide.getId(), shownPlaylistSlideIds);
        if (!isSuspended) {
            shownPlaylistSlideIds.push(playlistSlide.getId());
        }
        return isSuspended;
    });
    player.setSuspendPlayingFunction(closeWindow);
    player.play();
}

module.exports = {
    playPlaylist
};
