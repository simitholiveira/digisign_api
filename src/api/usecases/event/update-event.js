const EventRepository = require('@repos/event-repository');
const { setRequestDataToEvent } = require('@services/event/event-config-service');
const { assertEventScheduleDoesntOverlapStored } = require('@api/validations/schedule-validator');
const { getPlaylistByContentTypeIfNotExistsCreate } = require('@services/event/event-content-type');


async function updateEvent(request) {
    const event = await EventRepository.findById(request.params.id);
    setRequestDataToEvent(request.body, event);
    await assertEventScheduleDoesntOverlapStored(event);

    if (request.body.hasOwnProperty('content_type')) {
        const playlist = await getPlaylistByContentTypeIfNotExistsCreate(request.body.content_type);
        event.setPlaylist(playlist);
    }

    await EventRepository.update(event);
}


module.exports = {
    updateEvent,
};
