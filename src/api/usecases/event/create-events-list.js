const EventRepository = require('@repos/event-repository');
const { setRequestDataToEvent } = require('@services/event/event-config-service');
const Event = require('@models/event-model');
const { assertEventsListSchedulesNotOverlapped } = require('@api/validations/schedule-validator');
const { getPlaylistByContentTypeIfNotExistsCreate } = require('@services/event/event-content-type');


async function createEvents(request) {
    const events = createEventsByRequest(request);
    await assertEventsListSchedulesNotOverlapped(events);
    await bindPlaylistsToEvents(events);
    await EventRepository.reinsertBatch(events);
}

function createEventsByRequest(request) {
    const events = [];
    for (let attributes of request.body.events) {
        const event = new Event;
        event.setId(attributes.id);
        setRequestDataToEvent(attributes, event);
        events.push(event);
    }
    return events;
}

async function bindPlaylistsToEvents(events) {
    for (let event of events) {
        const playlist = await getPlaylistByContentTypeIfNotExistsCreate(
            event.getContentTypeAsJson()
        );
        event.setPlaylist(playlist);
    }
}

module.exports = {
    createEvents,
};
