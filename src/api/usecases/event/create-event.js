const EventRepository = require('@repos/event-repository');
const { setRequestDataToEvent } = require('@services/event/event-config-service');
const { getPlaylistByContentTypeIfNotExistsCreate } = require('@services/event/event-content-type');
const { isChildEvent } = require('@utils/event');
const Event = require('@models/event-model');
const { assertEventScheduleDoesntOverlapStored } = require('@api/validations/schedule-validator');


async function createEvent(request) {
    if (isChildEvent(request.body.event_pid)) {
        await createChildEvent(request);
    } else {
        await createParentEvent(request);
    }
}

async function createParentEvent(request) {
    const event = createEventByRequest(request);
    await assertEventScheduleDoesntOverlapStored(event);
    const playlist = await getPlaylistByContentTypeIfNotExistsCreate(request.body.content_type);
    event.setPlaylist(playlist);
    await EventRepository.insert(event);
}

function createEventByRequest(request) {
    const event = new Event;
    event.setId(request.body.id);
    setRequestDataToEvent(request.body, event);
    return event;
}

/**
 * TODO it will be done later.
 */
async function createChildEvent(request) {
    const parentEvent = await EventRepository.findById(request.body.event_pid);
}

module.exports = {
    createEvent,
};
