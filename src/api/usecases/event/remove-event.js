const EventRepository = require('@repos/event-repository');


async function removeEvent(request) {
    await EventRepository.deleteById(request.params.id);
}

module.exports = {
    removeEvent
};
