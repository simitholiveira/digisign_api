const User = require('../models/user-model');

class UserRepository {
    /**
     * @param {User} user
     */
    save(user) {
        if (user.id) {
            return User.query().updateAndFetchById(user.id, user);
        }
    }

    findByUsername(username) {
        return User.query().where({username}).first();
    }

    findByAccessToken(token) {
        return User.query().where({access_token: token}).first();
    }
}

module.exports = UserRepository;
