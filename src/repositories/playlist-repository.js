const Playlist = require('@models/playlist-model');
const PlaylistSlide = require('@models/playlist-slide-model');
const { DuplicateEntityError, NotFoundError } = require('@errors');

class PlaylistRepository {

    async save(playlist) {
        const playlistSlides = playlist.playlistSlides;
        if (!playlist.id) {
            playlist = await Playlist.query().insertAndFetch(playlist);
            playlist.playlistSlides = playlistSlides;
        }
        await this.savePlaylistSlides(playlist);
    }

    async savePlaylistSlides(playlist) {
        for (let index in playlist.playlistSlides) {
            let playlistSlide = playlist.playlistSlides[index];
            playlistSlide.playlist_id = playlist.id;
            await savePlaylistSlide(playlistSlide);
        }

        function savePlaylistSlide(playlistSlide) {
            const query = PlaylistSlide.query();
            if (!playlistSlide.id) {
                return query.insertAndFetch(playlistSlide);
            } else {
                return query.updateAndFetchById(playlistSlide.id, playlistSlide);
            }
        }
    }

    async deletePlaylistSlidesByPlaylistId(playlistId) {
        await PlaylistSlide.query().delete().where({playlist_id: playlistId});
    }

    static async findByExternalId(externalId) {
        const model = await Playlist.query()
            .withGraphFetched('playlistSlides(orderByPlaylistSlideId).slide.[files]')
            .modifiers({
                orderByPlaylistSlideId: builder => {
                    builder.orderBy('id');
                },
            })
            .where({external_id: externalId})
            .first();
        if (model) {
            return model;
        }
        throw new NotFoundError('Playlist not found.');
    }

    static async assertPlaylistExternalIdNotExist(externalId) {
        const model = await Playlist.query().where({external_id: externalId}).first();
        if (model) {
            throw new DuplicateEntityError('Playlist already used.');
        }
    }

    static async findById(id) {
        const model = await Playlist.query()
            .withGraphFetched('playlistSlides.slide.[files]')
            .where({id: id})
            .first();
        if (model) {
            return model;
        }
        throw new NotFoundError('Playlist not found.');
    }

    static findAll() {
        return Playlist.query()
            .withGraphFetched('playlistSlides.slide.[files]');
    }
}

module.exports = PlaylistRepository;
