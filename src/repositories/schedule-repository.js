const { fn } = require('objection');
const Schedule = require('../models/schedule-model');
const {dateToDbDate} = require('@utils/date');

class ScheduleRepository {
    /**
     * @param {Schedule} schedule
     */
    save(schedule) {
        if (!schedule.id) {
            return Schedule.query().insertAndFetch(schedule);
        }
    }

    /**
     * @param {Date} beforeDate
     * @returns {Objection.QueryBuilder<this, this[]>}
     */
    findAllUpcoming(beforeDate) {
        beforeDate = dateToDbDate(beforeDate);
        return Schedule.query()
            .whereBetween('start_at', [fn.now(), beforeDate])
            .orderBy('start_at');
    }

    findById(id) {
        return Schedule.query().findById(id);
    }
}

module.exports = ScheduleRepository;
