const pick = require('lodash/pick');
const Slide = require('@models/slide-model');
const FileRepository = require('./file-repository');
const {
    FileStorageRepository,
    getSlideBasePath,
} = require('./file-storage-repository');
const {isEmpty} = require('@utils');
const { DuplicateEntityError, NotFoundError } = require('@errors');


class SlideRepository {

    async save(slide) {
        const files = slide.files;
        const data = pick(slide, Slide.attributes);
        if (!slide.id) {
            slide = await Slide.query().insertAndFetch(data);
            slide.files = files;
        }
        await this.saveFiles(slide);
    }

    async update(slide) {
        await Slide.query().update(slide).where('id', slide.getId());
        await this.saveFiles(slide);
    }

    async saveFiles(slide) {
        const repo = new FileRepository;
        const storage = new FileStorageRepository(getSlideBasePath(slide));
        for (let index in slide.files) {
            let file = slide.files[index];
            if (!isEmpty(file.path)) {
                storage.save(file);
            }
            file.slide_id = slide.id;
            slide.files[index] = await repo.save(file);
        }
    }

    static async findByExternalId(externalId) {
        const model = await Slide.query()
            .withGraphFetched('files')
            .where({external_id: externalId})
            .first();
        if (model) {
            return model;
        }
        throw new NotFoundError('Slide not found.');
    }

    static async assertSlideExternalIdNotExist(externalId) {
        const model = await Slide.query().where({external_id: externalId}).first();
        if (model) {
            throw new DuplicateEntityError('Slide already used.');
        }
    }

    findById(id) {
        return Slide.query()
            .withGraphFetched('files')
            .where({id: id})
            .first();
    }

    static findAll() {
        return Slide.query()
            .withGraphFetched('files');
    }
}

module.exports = SlideRepository;
