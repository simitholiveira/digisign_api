const fs = require('fs');

const {isEmpty} = require('../util/index');
const config = require('../../config');

class FileStorageRepository {

    constructor(basePath) {
        this.basePath = basePath;
    }

    save(file) {
        if (isEmpty(file.path)) {
            throw new Error('File path is empty');
        }
        this.createDir(this.basePath);
        fs.copyFileSync(file.path, `${this.basePath}/${file.system_name}`);
    }

    createDir(path) {
        if (fs.existsSync(path)) {
            return;
        }
        fs.mkdirSync(path);
    }
}

function getSlideBasePath(slide) {
    return `${config.storageBasePath}/${slide.getSlideDirName()}`;
}

function getSlideBaseUrl(slide) {
    return `${config.storageBaseUrl}/${slide.getSlideDirName()}`;
}

module.exports = {
    FileStorageRepository,
    getSlideBasePath,
    getSlideBaseUrl,
};
