const Event = require('@models/event-model');
const { DuplicateEntityError, NotFoundError } = require('@errors');
const { dateToDbDate } = require('@utils/date');


class EventRepository {

    static async save(event) {
        const model = await Event.query().findById(event.getId());
        if (model) {
            await EventRepository.update(event);
        } else {
            await EventRepository.insert(event);
        }
    }

    static async insert(event) {
        await Event.query().insert(event);
    }

    static async reinsertBatch(events) {
        await Event.query().truncate();
        await EventRepository.insertBatch(events);
    }

    static async insertBatch(events) {
        await Event.knex().insert(events).into('event');
    }

    static async update(event) {
        await Event.query().update(event).where('id', event.getId());
    }

    static async assertEventIdNotExist(id) {
        const model = await Event.query().findById(id);
        if (model) {
            throw new DuplicateEntityError('Event already used.');
        }
    }

    static async findById(id) {
        const model = await Event.query()
            .withGraphFetched('playlist')
            .findById(id);
        if (model) {
            return model;
        }
        throw new NotFoundError('Event not found.');
    }

    static async findAllOverlappedByEvent(event) {
        const models = await EventRepository.findAllOverlappedByStartEndDates(
            event.getStartAt(),
            event.getEndAt()
        );
        return models;
    }

    static async findAllOverlappedByStartEndDates(startAt, endAt) {
        const models = await Event
            .query()
            .where('start_date', '<=', endAt)
            .andWhere('end_date', '>=', startAt);
        return models;
    }

    static async findAllPotentialUpcomingByDate(date) {

        date = dateToDbDate(date);

        const models = await Event.query()
            .withGraphFetched('playlistSlides(orderByPlaylistSlideId).slide.[files]')
            .modifiers({
                orderByPlaylistSlideId: builder => {
                    builder.orderBy('id');
                },
            })
            .where('end_date', '>', date);

        return models;
    }

    static async findAllPotentialUpcomingByDatePrev(date) {

        date = dateToDbDate(date);

        const models = await Event.query()
            .withGraphFetched('playlistSlides(orderByPlaylistSlideId).slide.[files]')
            .modifiers({
                orderByPlaylistSlideId: builder => {
                    builder.orderBy('id');
                },
            })
            .where('start_date', '<=', date)
            .andWhere('end_date', '>=', date);

        return models;
    }

    static async findByIdWithGraphFetched(id) {
        const model = await Event.query()
            .withGraphFetched('playlistSlides.slide.[files]')
            .findById(id);
        if (model) {
            return model;
        }
        throw new NotFoundError('Event not found');
    }

    static async deleteById(id) {
        await Event.query().deleteById(id);
    }

    static findAll() {
        return Event.query().withGraphFetched('playlist');
    }
}

module.exports = EventRepository;
