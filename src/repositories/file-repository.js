const pick = require('lodash/pick');

const File = require('../models/file-model');

class FileRepository {
    /**
     * @param {File} file
     */
    save(file) {
        const data = pick(file, File.attributes);
        if (!file.id) {
            return File.query().insertAndFetch(data);
        } else {
            return File.query().updateAndFetchById(file.id, data);
        }
    }
}

module.exports = FileRepository;
